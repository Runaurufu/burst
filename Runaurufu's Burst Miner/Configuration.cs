﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Runaurufu.Burst.Miner
{

  public class MinerConfiguration : IXmlSerializable
  {
    public String WalletAddress { get; set; }
    public String Passphrase { get; set; }
    public List<String> PlotDirectories { get; set; }
    public UInt64 MaxDeadlineCommitAllowed { get; set; }

    public MinerConfiguration()
    {
      this.PlotDirectories = new List<string>();
    }

    public System.Xml.Schema.XmlSchema GetSchema()
    {
      return null;
    }

    public void ReadXml(System.Xml.XmlReader reader)
    {
      bool wasEmpty = reader.IsEmptyElement;
      reader.Read();

      if (wasEmpty)
        return;

      reader.ReadStartElement("__version__");
      int serializationVersion = reader.ReadContentAsInt();
      reader.ReadEndElement();

      reader.ReadStartElement("WalletAddress");
      this.WalletAddress = reader.ReadString();
      reader.ReadEndElement();

      reader.ReadStartElement("Passphrase");
      this.Passphrase = reader.ReadString();
      reader.ReadEndElement();

      if (reader.IsStartElement() && reader.IsEmptyElement)
        reader.Read();
      else
      {
        reader.ReadStartElement("PlotDirectories");
        this.PlotDirectories = new List<string>();
        while (reader.IsStartElement())
        {
          reader.ReadStartElement("PlotDirectory");
          string directory = reader.ReadString();
          this.PlotDirectories.Add(directory);
          reader.ReadEndElement();
        }
        reader.ReadEndElement();
      }

      if (serializationVersion >= 2)
      {
        reader.ReadStartElement("MaxDeadlineCommitAllowed");
        this.MaxDeadlineCommitAllowed = (UInt64)reader.ReadContentAs(typeof(UInt64), null);
        reader.ReadEndElement();
      }

      reader.ReadEndElement();
    }

    public void WriteXml(System.Xml.XmlWriter writer)
    {
      XmlSerializer intSerializer = new XmlSerializer(typeof(int));

      int serializationVersion = 2;
      writer.WriteStartElement("__version__");
      writer.WriteValue(serializationVersion);
      writer.WriteEndElement();

      writer.WriteStartElement("WalletAddress");
      writer.WriteString(this.WalletAddress);
      writer.WriteEndElement();

      writer.WriteStartElement("Passphrase");
      writer.WriteString(this.Passphrase);
      writer.WriteEndElement();

      writer.WriteStartElement("PlotDirectories");
      if (this.PlotDirectories != null)
      {
        foreach (string directory in this.PlotDirectories)
        {
          writer.WriteStartElement("PlotDirectory");
          writer.WriteString(directory);
          writer.WriteEndElement();
        }
      }
      writer.WriteEndElement();

      writer.WriteStartElement("MaxDeadlineCommitAllowed");
      writer.WriteString(this.MaxDeadlineCommitAllowed.ToString());
      writer.WriteEndElement();
    }
  }

  public class Configuration : IXmlSerializable
  {
    public List<string> Wallets { get; set; }
    public List<MinerConfiguration> Miners { get; set; }

    /// <summary>
    /// If enabled then only one thread per miner can in time unit read data from same plot directory
    /// </summary>
    public bool SinglePlotDirectoryRead { get; set; }
    /// <summary>
    /// 
    /// </summary>
    public Int32 MaxThreadsPerMiner { get; set; }
    /// <summary>
    /// Cannot be greater then MaxThreadsPerMiner.
    /// </summary>
    public Int32 MaxThreadsPerMinerPlotDirectory { get; set; }
    /// <summary>
    /// Cannot be greater then MaxThreadsPerMinerPlotDirectory.
    /// </summary>
    public Int32 MaxThreadsPerMinerPlotFile { get; set; }
    /// <summary>
    /// Minimum size of plot chunk required for separate thread launch. Value in nonces. Set to 0 to disable.
    /// </summary>
    public Int64 MinerPlotFileThreadMinimumChunkSize { get; set; }
    /// <summary>
    /// Minimum duration of wallet readMiningInfo - sendCommits cycle. Must be non negative.
    /// </summary>
    public Int32 MinimumCycleDuration { get; set; }
    /// <summary>
    /// Total buffer size used by all threads. Value in nonces. Set to 0 to disable.
    /// </summary>
    public Int64 MaxReadBufferSize { get; set; }

    public Configuration()
    {
      this.Wallets = new List<string>();
      this.Miners = new List<MinerConfiguration>();
    }

    public System.Xml.Schema.XmlSchema GetSchema()
    {
      return null;
    }

    public void ReadXml(System.Xml.XmlReader reader)
    {
      XmlSerializer minerConfigurationSerializer = new XmlSerializer(typeof(MinerConfiguration));

      bool wasEmpty = reader.IsEmptyElement;
      reader.Read();

      if (wasEmpty)
        return;

      reader.ReadStartElement("__version__");
      int serializationVersion = reader.ReadContentAsInt();
      reader.ReadEndElement();

      if (reader.IsStartElement() && reader.IsEmptyElement)
        reader.Read();
      else
      {
        reader.ReadStartElement("Wallets");
        this.Wallets = new List<string>();
        while (reader.IsStartElement())
        {
          reader.ReadStartElement("Wallet");
          String wallet = reader.ReadString();
          this.Wallets.Add(wallet);
          reader.ReadEndElement();
        }
        reader.ReadEndElement();
      }

      if (reader.IsStartElement() && reader.IsEmptyElement)
        reader.Read();
      else
      {
        reader.ReadStartElement("Miners");
        this.Miners = new List<MinerConfiguration>();
        while (reader.IsStartElement())
        {
          //   reader.ReadStartElement("Miner");
          MinerConfiguration minerConfiguration = (MinerConfiguration)minerConfigurationSerializer.Deserialize(reader);
          this.Miners.Add(minerConfiguration);
          //   reader.ReadEndElement();
        }
        reader.ReadEndElement();
      }

      reader.ReadStartElement("SinglePlotDirectoryRead");
      this.SinglePlotDirectoryRead = reader.ReadContentAsBoolean();
      reader.ReadEndElement();

      reader.ReadStartElement("MaxThreadsPerMiner");
      this.MaxThreadsPerMiner = reader.ReadContentAsInt();
      reader.ReadEndElement();

      reader.ReadStartElement("MaxThreadsPerMinerPlotDirectory");
      this.MaxThreadsPerMinerPlotDirectory = reader.ReadContentAsInt();
      reader.ReadEndElement();

      if (serializationVersion >= 2)
      {
        reader.ReadStartElement("MaxThreadsPerMinerPlotFile");
        this.MaxThreadsPerMinerPlotFile = reader.ReadContentAsInt();
        reader.ReadEndElement();

        reader.ReadStartElement("MinerPlotFileThreadMinimumChunkSize");
        this.MinerPlotFileThreadMinimumChunkSize = reader.ReadContentAsLong();
        reader.ReadEndElement();

        reader.ReadStartElement("MinimumCycleDuration");
        this.MinimumCycleDuration = reader.ReadContentAsInt();
        reader.ReadEndElement();
      }

      reader.ReadStartElement("MaxTotalReadBufferSize");
      this.MaxReadBufferSize = reader.ReadContentAsLong();
      reader.ReadEndElement();

      reader.ReadEndElement();
    }

    public void WriteXml(System.Xml.XmlWriter writer)
    {
      XmlSerializer minerConfigurationSerializer = new XmlSerializer(typeof(MinerConfiguration));

      int serializationVersion = 2;
      writer.WriteStartElement("__version__");
      writer.WriteValue(serializationVersion);
      writer.WriteEndElement();

      writer.WriteStartElement("Wallets");
      if (this.Wallets != null)
      {
        foreach (string wallet in this.Wallets)
        {
          writer.WriteStartElement("Wallet");
          writer.WriteString(wallet);
          writer.WriteEndElement();
        }
      }
      writer.WriteEndElement();

      writer.WriteStartElement("Miners");
      if (this.Miners != null)
      {
        foreach (MinerConfiguration minerConfiguration in this.Miners)
        {
          //writer.WriteStartElement("Miner");
          minerConfigurationSerializer.Serialize(writer, minerConfiguration);
          //writer.WriteEndElement();
        }
      }
      writer.WriteEndElement();

      writer.WriteStartElement("SinglePlotDirectoryRead");
      writer.WriteValue(this.SinglePlotDirectoryRead);
      writer.WriteEndElement();

      writer.WriteStartElement("MaxThreadsPerMiner");
      writer.WriteValue(this.MaxThreadsPerMiner);
      writer.WriteEndElement();

      writer.WriteStartElement("MaxThreadsPerMinerPlotDirectory");
      writer.WriteValue(this.MaxThreadsPerMinerPlotDirectory);
      writer.WriteEndElement();

      writer.WriteStartElement("MaxThreadsPerMinerPlotFile");
      writer.WriteValue(this.MaxThreadsPerMinerPlotFile);
      writer.WriteEndElement();

      writer.WriteStartElement("MinerPlotFileThreadMinimumChunkSize");
      writer.WriteValue(this.MinerPlotFileThreadMinimumChunkSize);
      writer.WriteEndElement();

      writer.WriteStartElement("MinimumCycleDuration");
      writer.WriteValue(this.MinimumCycleDuration);
      writer.WriteEndElement();

      writer.WriteStartElement("MaxTotalReadBufferSize");
      writer.WriteValue(this.MaxReadBufferSize);
      writer.WriteEndElement();
    }
  }
}
