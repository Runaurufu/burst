﻿using Runaurufu.Burst.Library;
using Runaurufu.Burst.Library.Mining;
using Runaurufu.Threading;
using Runaurufu.Serialization;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Runaurufu.Burst.Miner
{
  class Program
  {
    static void Main(string[] args)
    {
      AppDomain.CurrentDomain.UnhandledException += CurrentDomain_UnhandledException;

      Console.WriteLine("Runaurufu's Burst Miner");
      Console.WriteLine("Version: {0}", System.Windows.Forms.Application.ProductVersion);
      Console.WriteLine();
      Console.WriteLine("Author: Runaurufu");
      Console.WriteLine("Contact: burst@runaurufu.com");
      Console.WriteLine("Please donate to support development:");
      Console.WriteLine("[BURST] BURST-UGRX-8RYE-K6KF-5L35U");
      Console.WriteLine();

      Configuration currentConfiguration = null;
      String appDirPath = Path.GetDirectoryName(Path.GetFullPath(System.Windows.Forms.Application.ExecutablePath));

      String configFilePath = Path.Combine(appDirPath, "configuration.xml");

      Console.WriteLine("{0:HH:mm:ss}: Loading configuration file {1}", DateTime.Now, configFilePath);

      if (File.Exists(configFilePath) == true)
      {
        try
        {
          String xml = File.ReadAllText(configFilePath, Encoding.UTF8);
          XmlSerializationHelper.Deserialize<Configuration>(xml, out currentConfiguration);

          // serialize again to commit all possible changes into config file!
          System.Xml.XmlWriterSettings settings = new System.Xml.XmlWriterSettings();
          settings.Encoding = new UnicodeEncoding(false, false); // no BOM in a .NET string
          settings.Indent = true;
          settings.OmitXmlDeclaration = true;
          settings.IndentChars = "  ";
          settings.NamespaceHandling = System.Xml.NamespaceHandling.OmitDuplicates;

          XmlSerializationHelper.Serialize(currentConfiguration, settings, out xml);
          File.WriteAllText(configFilePath, xml, Encoding.UTF8);
        }
        catch (Exception ex)
        {
          Console.WriteLine("Configuration file could not be read (" + ex.Message + ")!");
          Console.Beep(16000, 500);
          return;
        }
      }
      else
      {
        try
        {
          currentConfiguration = new Configuration();
          currentConfiguration.Wallets.Add("yourWalletAddress.com:80");
          currentConfiguration.Miners.Add(new MinerConfiguration() { Passphrase = "passwordForSoloMining", WalletAddress = "yourWalletAddress.com:80", PlotDirectories = new List<string>() { @"C:\plot1", @"C:\plot2" } });

          String xml;
          XmlSerializationHelper.Serialize(currentConfiguration, out xml);
          File.WriteAllText(configFilePath, xml, Encoding.UTF8);
          Console.WriteLine("Configuration file did not exist - new configuration file was created!");
        }
        catch (Exception ex)
        {
          Console.WriteLine("Configuration file did not exist - new configuration cannot be created (" + ex.Message + ")!");
        }
        Console.Beep(5000, 500);
        return;
      }

#warning CHECK CONFIGURATION IF IT IS CORRECT

      MinerWalletManager manager = new MinerWalletManager();
      foreach (string walletAddress in currentConfiguration.Wallets)
      {
        MinerWallet wallet = new MinerWallet(walletAddress);
        wallet.MinimumCycleDuration = currentConfiguration.MinimumCycleDuration;
        manager.AddWallet(wallet);

        Console.WriteLine("{0:HH:mm:ss}: Wallet added: {1}", DateTime.Now, walletAddress);
      }

      foreach (MinerConfiguration minerConfiguration in currentConfiguration.Miners)
      {
        MinerWallet wallet = null;
        foreach (MinerWallet managedWallet in manager.Wallets)
        {
          if (managedWallet.Address.ToUpper() == minerConfiguration.WalletAddress.ToUpper())
          {
            wallet = managedWallet;
            break;
          }
        }

        if (wallet == null)
        {
          wallet = new MinerWallet(minerConfiguration.WalletAddress);
          wallet.MinimumCycleDuration = currentConfiguration.MinimumCycleDuration;
          manager.AddWallet(wallet);

          Console.WriteLine("{0:HH:mm:ss}: Wallet added: {1}", DateTime.Now, wallet.Address);
        }

        Runaurufu.Burst.Library.Mining.Miner miner = new Runaurufu.Burst.Library.Mining.Miner(wallet, minerConfiguration.PlotDirectories.ToArray());
        miner.MaxWorkerThreads = currentConfiguration.MaxThreadsPerMiner;
        miner.MaxWorkerThreadsPerPlotDirectory = currentConfiguration.MaxThreadsPerMinerPlotDirectory;
        miner.MaxWorkerThreadsPerPlotFile = currentConfiguration.MaxThreadsPerMinerPlotFile;
        miner.MinimumWorkerChunkSize = (ulong)currentConfiguration.MinerPlotFileThreadMinimumChunkSize;
        miner.SinglePlotDirectoryRead = currentConfiguration.SinglePlotDirectoryRead;
        miner.MaxReadBufferSize = (ulong)currentConfiguration.MaxReadBufferSize;
        miner.MaxDeadlineCommitAllowed = minerConfiguration.MaxDeadlineCommitAllowed;

        wallet.AddMiner(miner);

        Console.WriteLine("{0:HH:mm:ss}: Miner for {1} wallet added", DateTime.Now, wallet.Address);
      }

      manager.NewMiningInfo += manager_NewMiningInfo;

      manager.StartMining();

      manager.Start();
      Console.WriteLine("{0:HH:mm:ss}: Mining started!", DateTime.Now);


      while (true)
      {
        System.Threading.Thread.Sleep(10000);
      }
    }

    static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
    {
      Console.WriteLine("{0:HH:mm:ss}: Application encountered exception", DateTime.Now);

      Exception ex = e.ExceptionObject as Exception;

      if (ex == null)
        return;

      Console.WriteLine(ex.ToString());
      Console.WriteLine();
      Console.WriteLine();

      string logFileName = Path.GetFullPath("RBMException.log");

      try
      {
        using (FileStream fs = new FileStream(logFileName, FileMode.Append, FileAccess.Write))
        {
          using (StreamWriter sw = new StreamWriter(fs))
          {
            sw.WriteLine("--- {0:yyyy.MM.dd HH:mm:ss} ---", DateTime.UtcNow);

            try
            {
              System.Reflection.Assembly assembly = System.Reflection.Assembly.GetExecutingAssembly();
              System.Reflection.AssemblyFileVersionAttribute version = assembly.GetCustomAttributes(typeof(System.Reflection.AssemblyFileVersionAttribute), true).FirstOrDefault() as System.Reflection.AssemblyFileVersionAttribute;
              if (version != null)
              {
                sw.WriteLine("Version: {0}", version.Version);
              }

              foreach (System.Reflection.AssemblyMetadataAttribute metadata in assembly.GetCustomAttributes(typeof(System.Reflection.AssemblyMetadataAttribute), true).Cast<System.Reflection.AssemblyMetadataAttribute>())
              {
                sw.WriteLine("{0}: {1}", metadata.Key, metadata.Value);
              }
            }
            catch (Exception exe)
            {
              sw.WriteLine("Unable to obtain assembly info ({0})", exe.Message);
            }

            sw.WriteLine(ex.ToString());
            sw.WriteLine();
          }
        }

        Console.WriteLine("{0:HH:mm:ss}: Exception info logged in {1}", DateTime.Now, logFileName);
      }
      catch (Exception exe)
      {
        Console.WriteLine("{0:HH:mm:ss}: Unable to save error into file {1} ({2})", DateTime.Now, logFileName, exe.Message);
      }
    }

    static void manager_NewMiningInfo(MinerWallet wallet, MiningInfo info)
    {
      Console.WriteLine("{0:HH:mm:ss}: {1} is looking for new block: {2}", DateTime.Now, wallet.Address, info.Height);
    }
  }
}
