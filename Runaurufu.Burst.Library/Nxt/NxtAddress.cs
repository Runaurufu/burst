﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Runaurufu.Burst.Library.Nxt
{
  public class NxtAddressStub
  {
    private string addressName;
    private ulong addressId;

    public String AddressName { get { return this.addressName; } }
    public UInt64 AddressId { get { return this.addressId; } }

    public NxtAddressStub(string address)
    {
      NxtAddress adr = NxtAddress.Create(address);
      this.addressId = adr.GetAccountId();
      this.addressName = adr.GetAccountName(true);
    }

    public NxtAddressStub(ulong addressId)
    {
      NxtAddress adr = NxtAddress.Create(addressId);
      this.addressId = adr.GetAccountId();
      this.addressName = adr.GetAccountName(true);
    }
  }

  public class NxtAddress
  {
    private readonly static string alphabet = "23456789ABCDEFGHJKLMNPQRSTUVWXYZ";
    private readonly static int[] cwmap = { 3, 2, 1, 0, 7, 6, 5, 4, 13, 14, 15, 16, 12, 8, 9, 10, 11, };
    private readonly static int[] gexp = { 1, 2, 4, 8, 16, 5, 10, 20, 13, 26, 17, 7, 14, 28, 29, 31, 27, 19, 3, 6, 12, 24, 21, 15, 30, 25, 23, 11, 22, 9, 18, 1 };
    private readonly static int[] glog = { 0, 0, 1, 18, 2, 5, 19, 11, 3, 29, 6, 27, 20, 8, 12, 23, 4, 10, 30, 17, 7, 22, 28, 26, 21, 25, 9, 16, 13, 14, 24, 15 };

    private char[] codeword = new char[17];
    private char[] syndrome = new char[5];

    private NxtAddress()
    {

    }

    public static NxtAddress Create(ulong id)
    {
      NxtAddress adr = new NxtAddress();
      adr.Set(id);
      return adr;
    }

    public static NxtAddress Create(string address)
    {
      if (address == null)
        throw new ArgumentNullException("address", "Address cannot be null");

      NxtAddress adr = new NxtAddress();
      if (adr.Set(address) == false)
        throw new ArgumentException("Specified address " +  address + " is not correct", "address");

      return adr;
    }

    public override string ToString()
    {
      return this.GetAccountName(true);
    }

    public ulong GetAccountId()
    {
      ulong acc = 0;

      for (int i = 0; i < 13; i++)
      {
        acc |= (ulong)(this.codeword[i] & 31) << (5 * i);
      }

      return acc;
    }

    public string GetAccountName(bool prefix)
    {
      char[] output = new char[20];
      int pos = 0;

      for (int i = 0; i < 17; i++)
      {
        output[pos++] = alphabet[(int)codeword[(int)cwmap[i]]];

        if ((i & 3) == 3 && i < 13) output[pos++] = '-';
      }

      if (prefix)
        return "BURST-" + new String(output);
      else
        return new String(output);
    }

    private int strspn(string InputString, char[] Mask)
    {
      int count = 0;
      foreach (var c in InputString)
      {
        if (!Mask.Contains(c)) break;
        count++;
      }

      return count;
    }

    private void Set(UInt64 id)
    {
      for (int i = 0; i < 13; i++)
      {
        this.codeword[i] = (char)((id >> (5 * i)) & 31);
      }

      this.encode();
    }

    private bool Set(string address)
    {

      int len = 0;
      for (int i = 0; i < 17; i++)
      {
        codeword[i] = (char)1;
      }

      char[] adr = null;
      if (address.StartsWith("BURST-", StringComparison.InvariantCultureIgnoreCase))
        adr = address.Substring(6).ToCharArray();
      else
        adr = address.ToCharArray();

      int digits = strspn(address, "0123456789 \t".ToCharArray());

      if (adr.Length == digits) // account id
      {
        if (digits > 5 && digits < 21)
        {
          if (digits == 20 && adr[0] != '1') return false;

          ulong acc;
          if (ulong.TryParse(new String(adr), out acc))
          {
            this.Set(acc);
            return true;
          }
        }
      }
      else // address
      {
        int index = 0;
        while (index < adr.Length)
        {
          char c = adr[index];
          index++;

          if (c >= 'a' && c <= 'z')
            c -= (char)('a' - 'A');

          int p = alphabet.IndexOf(c);

          if (p < 0)
            continue;

          if (len > 16)
            return false;

          codeword[cwmap[len++]] = (char)p;// alphabet[p];

        }
      }

      return (len == 17 ? this.Ok() : false);
    }

    void encode()
    {
      codeword[13] = (char)0;
      codeword[14] = (char)0;
      codeword[15] = (char)0;
      codeword[16] = (char)0;

      for (int i = 12; i >= 0; i--)
      {
        int fb = codeword[i] ^ codeword[16];

        codeword[16] = (char)(codeword[15] ^ gmult(30, fb));
        codeword[15] = (char)(codeword[14] ^ gmult(6, fb));
        codeword[14] = (char)(codeword[13] ^ gmult(9, fb));
        codeword[13] = (char)gmult(17, fb);
      }
    }

    int gmult(int a, int b)
    {
      if (a == 0 || b == 0) return 0;

      int idx = (glog[a] + glog[b]) % 31;

      return gexp[idx];
    }

    bool Ok()
    {
      int sum = 0;
      int t = 0;

      for (int i = 1; i < 5; i++)
      {
        t = 0;
        for (int j = 0; j < 31; j++)
        {
          if (j > 12 && j < 27) continue;

          int pos = j;
          if (j > 26)
            pos -= 14;

          t ^= gmult(codeword[pos], gexp[(i * j) % 31]);
        }

        sum |= t;
        syndrome[i] = (char)t;
      }

      return (sum == 0);
    }
  }
}
