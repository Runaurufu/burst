﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Runaurufu.Burst.Library
{
  /// <summary>
  /// Helper class for Burst library
  /// </summary>
  public static class BurstHelper
  {
    /// <summary>
    /// Size of each hash in bytes
    /// </summary>
    public const Int64 HashSize = 32;
    /// <summary>
    /// That many scoops are in single plot
    /// </summary>
    public const Int64 ScoopPerPlot = 4096;
    /// <summary>
    /// That many hashes are in single scoop
    /// </summary>
    public const Int64 HashPerScoop = 2;
    /// <summary>
    /// Size of each scoop in bytes
    /// </summary>
    public const Int64 ScoopSize = HashPerScoop * HashSize; // 64 Bytes
    /// <summary>
    /// Size of each plot (nonce) in bytes
    /// </summary>
    public const Int64 PlotSize = ScoopPerPlot * ScoopSize; // 256KB = 262144 Bytes
    /// <summary>
    /// ???
    /// </summary>
    // public const Int64 PlotScoopSize = ScoopSize + HashSize; // 64 + 32 bytes

    /// <summary>
    /// 
    /// </summary>
    /// <param name="deadline"></param>
    /// <returns></returns>
    public static string GetFormatedDeadline(UInt64 deadline)
    {
      ulong sec = deadline;
      ulong min = 0;
      ulong hour = 0;
      ulong day = 0;
      ulong year = 0;

      if (sec > 59)
      {
        min = sec / 60;
        sec = sec - min * 60;
      }

      if (min > 59)
      {
        hour = min / 60;
        min = min - hour * 60;
      }

      if (hour > 23)
      {
        day = hour / 24;
        hour = hour - day * 24;
      }

      if (day > 364)
      {
        year = day / 365;
        day = day - year * 365;
      }

      return string.Format("{0,6}y {1,3}d {2,2:D2}:{3,2:D2}:{4,2:D2}", year, day, hour, min, sec);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="shabal">State of miner shabal instance will be changed!</param>
    /// <param name="genSigData"></param>
    /// <param name="nonceData"></param>
    /// <param name="dataIndex"></param>
    /// <param name="baseTarget"></param>
    /// <returns></returns>
    public static ulong CalculateDeadline(ref Runaurufu.Burst.Library.Miner.MinerShabal shabal, byte[] genSigData, byte[] nonceData, int dataIndex, ulong baseTarget)
    {
      shabal.Update(genSigData, 0, (int)BurstHelper.HashSize);
      shabal.Update(nonceData, dataIndex, (int)BurstHelper.ScoopSize);

      byte[] output;
      shabal.Close(out output);

      UInt64 targetResult = BitConverter.ToUInt64(output, 0);

      //Shabal.Shabal256 md = new Shabal.Shabal256();
      //md.Update(genSigData);
      //md.Update(nonceData, 0, (int)BurstHelper.ScoopSize);
      //byte[] oo = md.Digest();

      //UInt64 stargetResult = BitConverter.ToUInt64(output, 0);

      //  memcpy(&targetResult, &target[0], sizeof(uint64_t));
      return targetResult / baseTarget;// this->miner->getBaseTarget();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="genSigData"></param>
    /// <param name="nonceData"></param>
    /// <param name="baseTarget"></param>
    /// <returns></returns>
    public static ulong CalculateDeadline(byte[] genSigData, byte[] nonceData, ulong baseTarget)
    {
      Runaurufu.Burst.Library.Miner.MinerShabal shabal = new Library.Miner.MinerShabal();

      return CalculateDeadline(ref shabal, genSigData, nonceData, 0, baseTarget);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="genSigString"></param>
    /// <returns></returns>
    public static byte[] CalculateGenSig(string genSigString)
    {
      byte[] genSig = new byte[BurstHelper.HashSize];
      for (int i = 0; i < BurstHelper.HashSize; i++)
      {
        string byteStr = genSigString.Substring(i * 2, 2);
        //   std::string byteStr = gensigStr.substr(i*2,2);
        genSig[i] = Convert.ToByte(byteStr, 16);
        // this->gensig[i] = (uint8_t)std::stoi(byteStr,0,16);
      }
      return genSig;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="genSig"></param>
    /// <param name="blockHeight"></param>
    /// <param name="baseTarget"></param>
    /// <returns></returns>
    public static int CalculateScoopNumber(byte[] genSig, UInt64 blockHeight, UInt64 baseTarget)
    {
      byte[] digest = new byte[BurstHelper.HashSize];
      Runaurufu.Burst.Library.Miner.MinerShabal shabal = new Library.Miner.MinerShabal();
      shabal.Update(genSig, 0, genSig.Length);
      shabal.Update(blockHeight);
      shabal.Close(out digest);

      return ((int)(digest[digest.Length - 2] & 0x0F) << 8) | (int)digest[digest.Length - 1];
    }
  }
}
