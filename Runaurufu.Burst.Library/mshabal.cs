﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Runaurufu.Burst.Library
{
  public class shabal_context
  {
    char[] buf; // 64
    UInt64 ptr;
    UInt32[] state; //[(12 + 16 + 16)];
    UInt32 Whigh, Wlow;
    uint out_size;
  }

  public class mshabal_context
  {
    char[] buf0; // 64
    char[] buf1; // 64
    char[] buf2; // 64
    char[] buf3; // 64
    UInt64 ptr;
    UInt32[] state; //[(12 + 16 + 16) * 4];
    UInt32 Whigh, Wlow;
    uint out_size;
  }

  public class mshabal256_context
  {
    char[] buf0; // 64
    char[] buf1; // 64
    char[] buf2; // 64
    char[] buf3; // 64
    char[] buf4; // 64
    char[] buf5; // 64
    char[] buf6; // 64
    char[] buf7; // 64
    UInt64 ptr;
    UInt32[] state; //[(12 + 16 + 16) * 4 * MSHABAL256_FACTOR];
    UInt32 Whigh, Wlow;
    uint out_size;
  }

  public static class Constants
  {
    public const int MSHABAL256_FACTOR = 2;
    public const int PLOT_SIZE = (4096 * 64);
    public const int HASH_SIZE = 32;
    public const int HASH_CAP = 4096;
    public const int WRITE_BLOCK_SIZE = (1024 * 1024 * 10);

    public const int HASHSIZE = 256;
    public const int HASHBYTES = (HASHSIZE / 8);
  }

  public class Nonce
  {
    public static void SetNonce(ref Char[] gendata, ref Char[] nonce)
    {
      gendata[Constants.PLOT_SIZE + 8] = nonce[7];
      gendata[Constants.PLOT_SIZE + 9] = nonce[6];
      gendata[Constants.PLOT_SIZE + 10] = nonce[5];
      gendata[Constants.PLOT_SIZE + 11] = nonce[4];
      gendata[Constants.PLOT_SIZE + 12] = nonce[3];
      gendata[Constants.PLOT_SIZE + 13] = nonce[2];
      gendata[Constants.PLOT_SIZE + 14] = nonce[1];
      gendata[Constants.PLOT_SIZE + 15] = nonce[0]
    }
  }
}
