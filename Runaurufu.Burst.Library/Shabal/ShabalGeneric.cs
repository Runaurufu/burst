﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Runaurufu.Burst.Library.Shabal
{
  /// <summary>
  /// 
  /// </summary>
  public class ShabalGeneric : IDigest
  {
    private int outSizeW32;
    private byte[] buf;
    private int ptr;
    private uint[] state;
    private long W;

    private ShabalGeneric()
    {
      buf = new byte[64];
      state = new uint[44];
    }

    /**
     * Create the object. The output size must be a multiple of 32,
     * between 32 and 512 (inclusive).
     *
     * @param outSize   the intended output size
     */
    public ShabalGeneric(int outSize)
      : this()
    {
      if (outSize < 32 || outSize > 512 || (outSize & 31) != 0)
        throw new ArgumentOutOfRangeException("invalid Shabal output size: " + outSize);
      outSizeW32 = TripleShift(outSize, 5);
      //outSizeW32 = outSize >>> 5;
      this.Reset();
    }

    /** @see Digest */
    public void Update(byte inbuf)
    {
      buf[ptr++] = inbuf;
      if (ptr == 64)
      {
        Core(buf, 0, 1);
        ptr = 0;
      }
    }

    /** @see Digest */
    public void Update(byte[] inbuf)
    {
      this.Update(inbuf, 0, inbuf.Length);
    }

    /** @see Digest */
    public void Update(byte[] inbuf, int off, int len)
    {
      if (ptr != 0)
      {
        int rlen = 64 - ptr;
        if (len < rlen)
        {
          Buffer.BlockCopy(inbuf, off, buf, ptr, len);
          //System.arraycopy(inbuf, off, buf, ptr, len);
          ptr += len;
          return;
        }
        else
        {
          Buffer.BlockCopy(inbuf, off, buf, ptr, rlen);
          //System.arraycopy(inbuf, off, buf, ptr, rlen);
          off += rlen;
          len -= rlen;
          Core(buf, 0, 1);
        }
      }
      int num = TripleShift(len, 6);// len >>> 6;
      if (num > 0)
      {
        Core(inbuf, off, num);
        off += num << 6;
        len &= 63;
      }
      Buffer.BlockCopy(inbuf, off, buf, 0, len);
      //System.arraycopy(inbuf, off, buf, 0, len);
      ptr = len;
    }

    /** @see Digest */
    public int GetDigestLength()
    {
      return outSizeW32 << 2;
    }

    /** @see Digest */
    public byte[] Digest()
    {
      int n = GetDigestLength();
      byte[] outBuf = new byte[n];
      Digest(outBuf, 0, n);
      return outBuf;
    }

    /** @see Digest */
    public byte[] Digest(byte[] inbuf)
    {
      Update(inbuf, 0, inbuf.Length);
      return Digest();
    }

    /** @see Digest */
    public int Digest(byte[] outbuf, int off, int len)
    {
      int dlen = GetDigestLength();
      if (len > dlen)
        len = dlen;
      buf[ptr++] = (byte)0x80;
      for (int i = ptr; i < 64; i++)
        buf[i] = 0;
      for (int i = 0; i < 4; i++)
      {
        Core(buf, 0, 1);
        W--;
      }
      uint j = 44 - (uint)TripleShift(dlen, 2);// (dlen >>> 2);
      uint w = 0;
      for (int i = 0; i < len; i++)
      {
        if ((i & 3) == 0)
          w = state[j++];
        outbuf[i + off] = (byte)w;

        w = TripleShift(w, 8);

        //w >>>= 8;
      }
      Reset();
      return len;
    }

    private static readonly uint[][] IVs = new uint[16][];

    // http://stackoverflow.com/questions/14817639/java-bit-operations-shift
    private static int TripleShift(int val, int shift)
    {
      return val >> shift;

      int n = val >> shift;

      int av = (int)((int)val >> (shift % 32));

      if (av != n)
      {

      }

      return (int)((uint)val >> (shift % 32));
    }

    private static long TripleShift(long val, int shift)
    {
      return val >> shift;

      long n = val >> shift;

      long av = (long)((ulong)val >> (shift % 32));

      long avu = val >> shift;

      if (av != n)
      {
        uint uu = 3846928793;
        uint dede = uu ^ (uint)n;
        uint deda = uu ^ (uint)av;
      }

      return n;

      return (long)((ulong)val >> (shift % 32));
    }

    private static uint TripleShift(uint val, int shift)
    {
      return val >> shift;

      uint n = val >> shift;

      uint av = (uint)(val >> (shift % 32));

      if (av != n)
      {

      }

      return (uint)(val >> (shift % 32));
    }

    private static uint[] GetIV(int outSizeW32)
    {
      uint[] iv = IVs[outSizeW32 - 1];
      if (iv == null)
      {
        int outSize = outSizeW32 << 5;
        ShabalGeneric sg = new ShabalGeneric();
        for (int i = 0; i < 44; i++)
          sg.state[i] = 0;
        sg.W = -1L;
        for (int i = 0; i < 16; i++)
        {
          sg.buf[(i << 2) + 0] =
            (byte)(outSize + i);
          sg.buf[(i << 2) + 1] = (byte)TripleShift(outSize + i, 8);
          //	(byte)((outSize + i) >>> 8);
        }
        sg.Core(sg.buf, 0, 1);
        for (int i = 0; i < 16; i++)
        {
          sg.buf[(i << 2) + 0] =
            (byte)(outSize + i + 16);
          sg.buf[(i << 2) + 1] = (byte)TripleShift(outSize + i + 16, 8);
          //	(byte)((outSize + i + 16) >>> 8);
        }
        sg.Core(sg.buf, 0, 1);
        iv = IVs[outSizeW32 - 1] = sg.state;
      }
      return iv;
    }

    /** @see Digest */
    public void Reset()
    {
      Array.Copy(GetIV(outSizeW32), 0, state, 0, 44);
      //	System.arraycopy(getIV(outSizeW32), 0, state, 0, 44);
      W = 1;
      ptr = 0;
    }

    /** @see Digest */
    public IDigest Copy()
    {
      ShabalGeneric d = Dup();
      d.outSizeW32 = outSizeW32;
      Buffer.BlockCopy(buf, 0, d.buf, 0, ptr);
      //	System.arraycopy(buf, 0, d.buf, 0, ptr);
      d.ptr = ptr;
      Buffer.BlockCopy(state, 0, d.state, 0, 44);
      //	System.arraycopy(state, 0, d.state, 0, 44);
      d.W = W;
      return d;
    }

    /**
     * Create a new instance with the same parameters. This method
     * is invoked from {@link #copy}.
     *
     * @return  the new instance
     */
    protected virtual ShabalGeneric Dup()
    {
      return new ShabalGeneric();
    }

    /** @see Digest */
    public int GetBlockLength()
    {
      return 64;
    }

    private uint[] M = new uint[16];

    private static uint DecodeLEInt(byte[] data, int off)
    {
      uint ret = BitConverter.ToUInt32(data, off);
      if (BitConverter.IsLittleEndian)
      {
        return ret;
      }
      else
      {
        return (ret & 0x000000FFU) << 24 | (ret & 0x0000FF00U) << 8 |
               (ret & 0x00FF0000U) >> 8 | (ret & 0xFF000000U) >> 24;
      }

      return (uint)((data[off + 0] & 0xFF)
        | ((data[off + 1] & 0xFF) << 8)
        | ((data[off + 2] & 0xFF) << 16)
        | ((data[off + 3] & 0xFF) << 24));
    }

    private void Core(byte[] data, int off, int num)
    {
      uint A0 = state[0];
      uint A1 = state[1];
      uint A2 = state[2];
      uint A3 = state[3];
      uint A4 = state[4];
      uint A5 = state[5];
      uint A6 = state[6];
      uint A7 = state[7];
      uint A8 = state[8];
      uint A9 = state[9];
      uint AA = state[10];
      uint AB = state[11];

      uint B0 = state[12];
      uint B1 = state[13];
      uint B2 = state[14];
      uint B3 = state[15];
      uint B4 = state[16];
      uint B5 = state[17];
      uint B6 = state[18];
      uint B7 = state[19];
      uint B8 = state[20];
      uint B9 = state[21];
      uint BA = state[22];
      uint BB = state[23];
      uint BC = state[24];
      uint BD = state[25];
      uint BE = state[26];
      uint BF = state[27];

      uint C0 = state[28];
      uint C1 = state[29];
      uint C2 = state[30];
      uint C3 = state[31];
      uint C4 = state[32];
      uint C5 = state[33];
      uint C6 = state[34];
      uint C7 = state[35];
      uint C8 = state[36];
      uint C9 = state[37];
      uint CA = state[38];
      uint CB = state[39];
      uint CC = state[40];
      uint CD = state[41];
      uint CE = state[42];
      uint CF = state[43];

      while (num-- > 0)
      {
        uint M0 = DecodeLEInt(data, off + 0);
        B0 += M0;
        B0 = (B0 << 17) | TripleShift(B0, 15);// (B0 >>> 15);
        uint M1 = DecodeLEInt(data, off + 4);
        B1 += M1;
        B1 = (B1 << 17) | TripleShift(B1, 15);//(B1 >>> 15);
        uint M2 = DecodeLEInt(data, off + 8);
        B2 += M2;
        B2 = (B2 << 17) | TripleShift(B2, 15);//(B2 >>> 15);
        uint M3 = DecodeLEInt(data, off + 12);
        B3 += M3;
        B3 = (B3 << 17) | TripleShift(B3, 15);//(B3 >>> 15);
        uint M4 = DecodeLEInt(data, off + 16);
        B4 += M4;
        B4 = (B4 << 17) | TripleShift(B4, 15);//(B4 >>> 15);
        uint M5 = DecodeLEInt(data, off + 20);
        B5 += M5;
        B5 = (B5 << 17) | TripleShift(B5, 15);//(B5 >>> 15);
        uint M6 = DecodeLEInt(data, off + 24);
        B6 += M6;
        B6 = (B6 << 17) | TripleShift(B6, 15);//(B6 >>> 15);
        uint M7 = DecodeLEInt(data, off + 28);
        B7 += M7;
        B7 = (B7 << 17) | TripleShift(B7, 15);//(B7 >>> 15);
        uint M8 = DecodeLEInt(data, off + 32);
        B8 += M8;
        B8 = (B8 << 17) | TripleShift(B8, 15);//(B8 >>> 15);
        uint M9 = DecodeLEInt(data, off + 36);
        B9 += M9;
        B9 = (B9 << 17) | TripleShift(B9, 15);//(B9 >>> 15);
        uint MA = DecodeLEInt(data, off + 40);
        BA += MA;
        BA = (BA << 17) | TripleShift(BA, 15);//(BA >>> 15);
        uint MB = DecodeLEInt(data, off + 44);
        BB += MB;
        BB = (BB << 17) | TripleShift(BB, 15);//(BB >>> 15);
        uint MC = DecodeLEInt(data, off + 48);
        BC += MC;
        BC = (BC << 17) | TripleShift(BC, 15);//(BC >>> 15);
        uint MD = DecodeLEInt(data, off + 52);
        BD += MD;
        BD = (BD << 17) | TripleShift(BD, 15);//(BD >>> 15);
        uint ME = DecodeLEInt(data, off + 56);
        BE += ME;
        BE = (BE << 17) | TripleShift(BE, 15);//(BE >>> 15);
        uint MF = DecodeLEInt(data, off + 60);
        BF += MF;
        BF = (BF << 17) | TripleShift(BF, 15);//(BF >>> 15);

        off += 64;
        A0 ^= (uint)W;
        A1 ^= (uint)TripleShift(W, 32);//(W >>> 32);
        W++;


        A0 = ((A0 ^ (((AB << 15) | TripleShift(AB, 17)) * 5) ^ C8) * 3) ^ BD ^ (B9 & ~B6) ^ M0;
        B0 = ~((B0 << 1) | TripleShift(B0, 31)) ^ A0;
        A1 = ((A1 ^ (((A0 << 15) | TripleShift(A0, 17)) * 5) ^ C7) * 3) ^ BE ^ (BA & ~B7) ^ M1;
        B1 = ~((B1 << 1) | TripleShift(B1, 31)) ^ A1;
        A2 = ((A2 ^ (((A1 << 15) | TripleShift(A1, 17)) * 5) ^ C6) * 3) ^ BF ^ (BB & ~B8) ^ M2;
        B2 = ~((B2 << 1) | TripleShift(B2, 31)) ^ A2;
        A3 = ((A3 ^ (((A2 << 15) | TripleShift(A2, 17)) * 5) ^ C5) * 3) ^ B0 ^ (BC & ~B9) ^ M3;
        B3 = ~((B3 << 1) | TripleShift(B3, 31)) ^ A3;
        A4 = ((A4 ^ (((A3 << 15) | TripleShift(A3, 17)) * 5) ^ C4) * 3) ^ B1 ^ (BD & ~BA) ^ M4;
        B4 = ~((B4 << 1) | TripleShift(B4, 31)) ^ A4;
        A5 = ((A5 ^ (((A4 << 15) | TripleShift(A4, 17)) * 5) ^ C3) * 3) ^ B2 ^ (BE & ~BB) ^ M5;
        B5 = ~((B5 << 1) | TripleShift(B5, 31)) ^ A5;
        A6 = ((A6 ^ (((A5 << 15) | TripleShift(A5, 17)) * 5) ^ C2) * 3) ^ B3 ^ (BF & ~BC) ^ M6;
        B6 = ~((B6 << 1) | TripleShift(B6, 31)) ^ A6;
        A7 = ((A7 ^ (((A6 << 15) | TripleShift(A6, 17)) * 5) ^ C1) * 3) ^ B4 ^ (B0 & ~BD) ^ M7;
        B7 = ~((B7 << 1) | TripleShift(B7, 31)) ^ A7;
        A8 = ((A8 ^ (((A7 << 15) | TripleShift(A7, 17)) * 5) ^ C0) * 3) ^ B5 ^ (B1 & ~BE) ^ M8;
        B8 = ~((B8 << 1) | TripleShift(B8, 31)) ^ A8;
        A9 = ((A9 ^ (((A8 << 15) | TripleShift(A8, 17)) * 5) ^ CF) * 3) ^ B6 ^ (B2 & ~BF) ^ M9;
        B9 = ~((B9 << 1) | TripleShift(B9, 31)) ^ A9;
        AA = ((AA ^ (((A9 << 15) | TripleShift(A9, 17)) * 5) ^ CE) * 3) ^ B7 ^ (B3 & ~B0) ^ MA;
        BA = ~((BA << 1) | TripleShift(BA, 31)) ^ AA;
        AB = ((AB ^ (((AA << 15) | TripleShift(AA, 17)) * 5) ^ CD) * 3) ^ B8 ^ (B4 & ~B1) ^ MB;
        BB = ~((BB << 1) | TripleShift(BB, 31)) ^ AB;
        A0 = ((A0 ^ (((AB << 15) | TripleShift(AB, 17)) * 5) ^ CC) * 3) ^ B9 ^ (B5 & ~B2) ^ MC;
        BC = ~((BC << 1) | TripleShift(BC, 31)) ^ A0;
        A1 = ((A1 ^ (((A0 << 15) | TripleShift(A0, 17)) * 5) ^ CB) * 3) ^ BA ^ (B6 & ~B3) ^ MD;
        BD = ~((BD << 1) | TripleShift(BD, 31)) ^ A1;
        A2 = ((A2 ^ (((A1 << 15) | TripleShift(A1, 17)) * 5) ^ CA) * 3) ^ BB ^ (B7 & ~B4) ^ ME;
        BE = ~((BE << 1) | TripleShift(BE, 31)) ^ A2;
        A3 = ((A3 ^ (((A2 << 15) | TripleShift(A2, 17)) * 5) ^ C9) * 3) ^ BC ^ (B8 & ~B5) ^ MF;
        BF = ~((BF << 1) | TripleShift(BF, 31)) ^ A3;
        A4 = ((A4 ^ (((A3 << 15) | TripleShift(A3, 17)) * 5) ^ C8) * 3) ^ BD ^ (B9 & ~B6) ^ M0;
        B0 = ~((B0 << 1) | TripleShift(B0, 31)) ^ A4;
        A5 = ((A5 ^ (((A4 << 15) | TripleShift(A4, 17)) * 5) ^ C7) * 3) ^ BE ^ (BA & ~B7) ^ M1;
        B1 = ~((B1 << 1) | TripleShift(B1, 31)) ^ A5;
        A6 = ((A6 ^ (((A5 << 15) | TripleShift(A5, 17)) * 5) ^ C6) * 3) ^ BF ^ (BB & ~B8) ^ M2;
        B2 = ~((B2 << 1) | TripleShift(B2, 31)) ^ A6;
        A7 = ((A7 ^ (((A6 << 15) | TripleShift(A6, 17)) * 5) ^ C5) * 3) ^ B0 ^ (BC & ~B9) ^ M3;
        B3 = ~((B3 << 1) | TripleShift(B3, 31)) ^ A7;
        A8 = ((A8 ^ (((A7 << 15) | TripleShift(A7, 17)) * 5) ^ C4) * 3) ^ B1 ^ (BD & ~BA) ^ M4;
        B4 = ~((B4 << 1) | TripleShift(B4, 31)) ^ A8;
        A9 = ((A9 ^ (((A8 << 15) | TripleShift(A8, 17)) * 5) ^ C3) * 3) ^ B2 ^ (BE & ~BB) ^ M5;
        B5 = ~((B5 << 1) | TripleShift(B5, 31)) ^ A9;
        AA = ((AA ^ (((A9 << 15) | TripleShift(A9, 17)) * 5) ^ C2) * 3) ^ B3 ^ (BF & ~BC) ^ M6;
        B6 = ~((B6 << 1) | TripleShift(B6, 31)) ^ AA;
        AB = ((AB ^ (((AA << 15) | TripleShift(AA, 17)) * 5) ^ C1) * 3) ^ B4 ^ (B0 & ~BD) ^ M7;
        B7 = ~((B7 << 1) | TripleShift(B7, 31)) ^ AB;
        A0 = ((A0 ^ (((AB << 15) | TripleShift(AB, 17)) * 5) ^ C0) * 3) ^ B5 ^ (B1 & ~BE) ^ M8;
        B8 = ~((B8 << 1) | TripleShift(B8, 31)) ^ A0;
        A1 = ((A1 ^ (((A0 << 15) | TripleShift(A0, 17)) * 5) ^ CF) * 3) ^ B6 ^ (B2 & ~BF) ^ M9;
        B9 = ~((B9 << 1) | TripleShift(B9, 31)) ^ A1;
        A2 = ((A2 ^ (((A1 << 15) | TripleShift(A1, 17)) * 5) ^ CE) * 3) ^ B7 ^ (B3 & ~B0) ^ MA;
        BA = ~((BA << 1) | TripleShift(BA, 31)) ^ A2;
        A3 = ((A3 ^ (((A2 << 15) | TripleShift(A2, 17)) * 5) ^ CD) * 3) ^ B8 ^ (B4 & ~B1) ^ MB;
        BB = ~((BB << 1) | TripleShift(BB, 31)) ^ A3;
        A4 = ((A4 ^ (((A3 << 15) | TripleShift(A3, 17)) * 5) ^ CC) * 3) ^ B9 ^ (B5 & ~B2) ^ MC;
        BC = ~((BC << 1) | TripleShift(BC, 31)) ^ A4;
        A5 = ((A5 ^ (((A4 << 15) | TripleShift(A4, 17)) * 5) ^ CB) * 3) ^ BA ^ (B6 & ~B3) ^ MD;
        BD = ~((BD << 1) | TripleShift(BD, 31)) ^ A5;
        A6 = ((A6 ^ (((A5 << 15) | TripleShift(A5, 17)) * 5) ^ CA) * 3) ^ BB ^ (B7 & ~B4) ^ ME;
        BE = ~((BE << 1) | TripleShift(BE, 31)) ^ A6;
        A7 = ((A7 ^ (((A6 << 15) | TripleShift(A6, 17)) * 5) ^ C9) * 3) ^ BC ^ (B8 & ~B5) ^ MF;
        BF = ~((BF << 1) | TripleShift(BF, 31)) ^ A7;
        A8 = ((A8 ^ (((A7 << 15) | TripleShift(A7, 17)) * 5) ^ C8) * 3) ^ BD ^ (B9 & ~B6) ^ M0;
        B0 = ~((B0 << 1) | TripleShift(B0, 31)) ^ A8;
        A9 = ((A9 ^ (((A8 << 15) | TripleShift(A8, 17)) * 5) ^ C7) * 3) ^ BE ^ (BA & ~B7) ^ M1;
        B1 = ~((B1 << 1) | TripleShift(B1, 31)) ^ A9;
        AA = ((AA ^ (((A9 << 15) | TripleShift(A9, 17)) * 5) ^ C6) * 3) ^ BF ^ (BB & ~B8) ^ M2;
        B2 = ~((B2 << 1) | TripleShift(B2, 31)) ^ AA;
        AB = ((AB ^ (((AA << 15) | TripleShift(AA, 17)) * 5) ^ C5) * 3) ^ B0 ^ (BC & ~B9) ^ M3;
        B3 = ~((B3 << 1) | TripleShift(B3, 31)) ^ AB;
        A0 = ((A0 ^ (((AB << 15) | TripleShift(AB, 17)) * 5) ^ C4) * 3) ^ B1 ^ (BD & ~BA) ^ M4;
        B4 = ~((B4 << 1) | TripleShift(B4, 31)) ^ A0;
        A1 = ((A1 ^ (((A0 << 15) | TripleShift(A0, 17)) * 5) ^ C3) * 3) ^ B2 ^ (BE & ~BB) ^ M5;
        B5 = ~((B5 << 1) | TripleShift(B5, 31)) ^ A1;
        A2 = ((A2 ^ (((A1 << 15) | TripleShift(A1, 17)) * 5) ^ C2) * 3) ^ B3 ^ (BF & ~BC) ^ M6;
        B6 = ~((B6 << 1) | TripleShift(B6, 31)) ^ A2;
        A3 = ((A3 ^ (((A2 << 15) | TripleShift(A2, 17)) * 5) ^ C1) * 3) ^ B4 ^ (B0 & ~BD) ^ M7;
        B7 = ~((B7 << 1) | TripleShift(B7, 31)) ^ A3;
        A4 = ((A4 ^ (((A3 << 15) | TripleShift(A3, 17)) * 5) ^ C0) * 3) ^ B5 ^ (B1 & ~BE) ^ M8;
        B8 = ~((B8 << 1) | TripleShift(B8, 31)) ^ A4;
        A5 = ((A5 ^ (((A4 << 15) | TripleShift(A4, 17)) * 5) ^ CF) * 3) ^ B6 ^ (B2 & ~BF) ^ M9;
        B9 = ~((B9 << 1) | TripleShift(B9, 31)) ^ A5;
        A6 = ((A6 ^ (((A5 << 15) | TripleShift(A5, 17)) * 5) ^ CE) * 3) ^ B7 ^ (B3 & ~B0) ^ MA;
        BA = ~((BA << 1) | TripleShift(BA, 31)) ^ A6;
        A7 = ((A7 ^ (((A6 << 15) | TripleShift(A6, 17)) * 5) ^ CD) * 3) ^ B8 ^ (B4 & ~B1) ^ MB;
        BB = ~((BB << 1) | TripleShift(BB, 31)) ^ A7;
        A8 = ((A8 ^ (((A7 << 15) | TripleShift(A7, 17)) * 5) ^ CC) * 3) ^ B9 ^ (B5 & ~B2) ^ MC;
        BC = ~((BC << 1) | TripleShift(BC, 31)) ^ A8;
        A9 = ((A9 ^ (((A8 << 15) | TripleShift(A8, 17)) * 5) ^ CB) * 3) ^ BA ^ (B6 & ~B3) ^ MD;
        BD = ~((BD << 1) | TripleShift(BD, 31)) ^ A9;
        AA = ((AA ^ (((A9 << 15) | TripleShift(A9, 17)) * 5) ^ CA) * 3) ^ BB ^ (B7 & ~B4) ^ ME;
        BE = ~((BE << 1) | TripleShift(BE, 31)) ^ AA;
        AB = ((AB ^ (((AA << 15) | TripleShift(AA, 17)) * 5) ^ C9) * 3) ^ BC ^ (B8 & ~B5) ^ MF;
        BF = ~((BF << 1) | TripleShift(BF, 31)) ^ AB;

        //A0 = ((A0 ^ (((AB << 15) | (AB >>> 17)) * 5) ^ C8) * 3) ^ BD ^ (B9 & ~B6) ^ M0;
        //A0 = ((A0 ^ (((AB << 15) | (AB >>> 17)) * 5) ^ C8) * 3) ^ BD ^ (B9 & ~B6) ^ M0;
        //B0 = ~((B0 << 1) | (B0 >>> 31)) ^ A0;
        //B0 = ~((B0 << 1) | (B0 >>> 31)) ^ A0;
        //A1 = ((A1 ^ (((A0 << 15) | (A0 >>> 17)) * 5) ^ C7) * 3)	^ BE ^ (BA & ~B7) ^ M1;
        //B1 = ~((B1 << 1) | (B1 >>> 31)) ^ A1;
        //A2 = ((A2 ^ (((A1 << 15) | (A1 >>> 17)) * 5) ^ C6) * 3) ^ BF ^ (BB & ~B8) ^ M2;
        //B2 = ~((B2 << 1) | (B2 >>> 31)) ^ A2;
        //A3 = ((A3 ^ (((A2 << 15) | (A2 >>> 17)) * 5) ^ C5) * 3)	^ B0 ^ (BC & ~B9) ^ M3;
        //B3 = ~((B3 << 1) | (B3 >>> 31)) ^ A3;
        //A4 = ((A4 ^ (((A3 << 15) | (A3 >>> 17)) * 5) ^ C4) * 3)	^ B1 ^ (BD & ~BA) ^ M4;
        //B4 = ~((B4 << 1) | (B4 >>> 31)) ^ A4;
        //A5 = ((A5 ^ (((A4 << 15) | (A4 >>> 17)) * 5) ^ C3) * 3)	^ B2 ^ (BE & ~BB) ^ M5;
        //B5 = ~((B5 << 1) | (B5 >>> 31)) ^ A5;
        //A6 = ((A6 ^ (((A5 << 15) | (A5 >>> 17)) * 5) ^ C2) * 3) ^ B3 ^ (BF & ~BC) ^ M6;
        //B6 = ~((B6 << 1) | (B6 >>> 31)) ^ A6;
        //A7 = ((A7 ^ (((A6 << 15) | (A6 >>> 17)) * 5) ^ C1) * 3)	^ B4 ^ (B0 & ~BD) ^ M7;
        //B7 = ~((B7 << 1) | (B7 >>> 31)) ^ A7;
        //A8 = ((A8 ^ (((A7 << 15) | (A7 >>> 17)) * 5) ^ C0) * 3)	^ B5 ^ (B1 & ~BE) ^ M8;
        //B8 = ~((B8 << 1) | (B8 >>> 31)) ^ A8;
        //A9 = ((A9 ^ (((A8 << 15) | (A8 >>> 17)) * 5) ^ CF) * 3)	^ B6 ^ (B2 & ~BF) ^ M9;
        //B9 = ~((B9 << 1) | (B9 >>> 31)) ^ A9;
        //AA = ((AA ^ (((A9 << 15) | (A9 >>> 17)) * 5) ^ CE) * 3)	^ B7 ^ (B3 & ~B0) ^ MA;
        //BA = ~((BA << 1) | (BA >>> 31)) ^ AA;
        //AB = ((AB ^ (((AA << 15) | (AA >>> 17)) * 5) ^ CD) * 3)	^ B8 ^ (B4 & ~B1) ^ MB;
        //BB = ~((BB << 1) | (BB >>> 31)) ^ AB;
        //A0 = ((A0 ^ (((AB << 15) | (AB >>> 17)) * 5) ^ CC) * 3)	^ B9 ^ (B5 & ~B2) ^ MC;
        //BC = ~((BC << 1) | (BC >>> 31)) ^ A0;
        //A1 = ((A1 ^ (((A0 << 15) | (A0 >>> 17)) * 5) ^ CB) * 3)	^ BA ^ (B6 & ~B3) ^ MD;
        //BD = ~((BD << 1) | (BD >>> 31)) ^ A1;
        //A2 = ((A2 ^ (((A1 << 15) | (A1 >>> 17)) * 5) ^ CA) * 3)	^ BB ^ (B7 & ~B4) ^ ME;
        //BE = ~((BE << 1) | (BE >>> 31)) ^ A2;
        //A3 = ((A3 ^ (((A2 << 15) | (A2 >>> 17)) * 5) ^ C9) * 3)	^ BC ^ (B8 & ~B5) ^ MF;
        //BF = ~((BF << 1) | (BF >>> 31)) ^ A3;
        //A4 = ((A4 ^ (((A3 << 15) | (A3 >>> 17)) * 5) ^ C8) * 3)	^ BD ^ (B9 & ~B6) ^ M0;
        //B0 = ~((B0 << 1) | (B0 >>> 31)) ^ A4;
        //A5 = ((A5 ^ (((A4 << 15) | (A4 >>> 17)) * 5) ^ C7) * 3)	^ BE ^ (BA & ~B7) ^ M1;
        //B1 = ~((B1 << 1) | (B1 >>> 31)) ^ A5;
        //A6 = ((A6 ^ (((A5 << 15) | (A5 >>> 17)) * 5) ^ C6) * 3)	^ BF ^ (BB & ~B8) ^ M2;
        //B2 = ~((B2 << 1) | (B2 >>> 31)) ^ A6;
        //A7 = ((A7 ^ (((A6 << 15) | (A6 >>> 17)) * 5) ^ C5) * 3)	^ B0 ^ (BC & ~B9) ^ M3;
        //B3 = ~((B3 << 1) | (B3 >>> 31)) ^ A7;
        //A8 = ((A8 ^ (((A7 << 15) | (A7 >>> 17)) * 5) ^ C4) * 3)	^ B1 ^ (BD & ~BA) ^ M4;
        //B4 = ~((B4 << 1) | (B4 >>> 31)) ^ A8;
        //A9 = ((A9 ^ (((A8 << 15) | (A8 >>> 17)) * 5) ^ C3) * 3)	^ B2 ^ (BE & ~BB) ^ M5;
        //B5 = ~((B5 << 1) | (B5 >>> 31)) ^ A9;
        //AA = ((AA ^ (((A9 << 15) | (A9 >>> 17)) * 5) ^ C2) * 3)	^ B3 ^ (BF & ~BC) ^ M6;
        //B6 = ~((B6 << 1) | (B6 >>> 31)) ^ AA;
        //AB = ((AB ^ (((AA << 15) | (AA >>> 17)) * 5) ^ C1) * 3)	^ B4 ^ (B0 & ~BD) ^ M7;
        //B7 = ~((B7 << 1) | (B7 >>> 31)) ^ AB;
        //A0 = ((A0 ^ (((AB << 15) | (AB >>> 17)) * 5) ^ C0) * 3)	^ B5 ^ (B1 & ~BE) ^ M8;
        //B8 = ~((B8 << 1) | (B8 >>> 31)) ^ A0;
        //A1 = ((A1 ^ (((A0 << 15) | (A0 >>> 17)) * 5) ^ CF) * 3)	^ B6 ^ (B2 & ~BF) ^ M9;
        //B9 = ~((B9 << 1) | (B9 >>> 31)) ^ A1;
        //A2 = ((A2 ^ (((A1 << 15) | (A1 >>> 17)) * 5) ^ CE) * 3)	^ B7 ^ (B3 & ~B0) ^ MA;
        //BA = ~((BA << 1) | (BA >>> 31)) ^ A2;
        //A3 = ((A3 ^ (((A2 << 15) | (A2 >>> 17)) * 5) ^ CD) * 3)	^ B8 ^ (B4 & ~B1) ^ MB;
        //BB = ~((BB << 1) | (BB >>> 31)) ^ A3;
        //A4 = ((A4 ^ (((A3 << 15) | (A3 >>> 17)) * 5) ^ CC) * 3)	^ B9 ^ (B5 & ~B2) ^ MC;
        //BC = ~((BC << 1) | (BC >>> 31)) ^ A4;
        //A5 = ((A5 ^ (((A4 << 15) | (A4 >>> 17)) * 5) ^ CB) * 3)	^ BA ^ (B6 & ~B3) ^ MD;
        //BD = ~((BD << 1) | (BD >>> 31)) ^ A5;
        //A6 = ((A6 ^ (((A5 << 15) | (A5 >>> 17)) * 5) ^ CA) * 3)	^ BB ^ (B7 & ~B4) ^ ME;
        //BE = ~((BE << 1) | (BE >>> 31)) ^ A6;
        //A7 = ((A7 ^ (((A6 << 15) | (A6 >>> 17)) * 5) ^ C9) * 3)	^ BC ^ (B8 & ~B5) ^ MF;
        //BF = ~((BF << 1) | (BF >>> 31)) ^ A7;
        //A8 = ((A8 ^ (((A7 << 15) | (A7 >>> 17)) * 5) ^ C8) * 3)	^ BD ^ (B9 & ~B6) ^ M0;
        //B0 = ~((B0 << 1) | (B0 >>> 31)) ^ A8;
        //A9 = ((A9 ^ (((A8 << 15) | (A8 >>> 17)) * 5) ^ C7) * 3)	^ BE ^ (BA & ~B7) ^ M1;
        //B1 = ~((B1 << 1) | (B1 >>> 31)) ^ A9;
        //AA = ((AA ^ (((A9 << 15) | (A9 >>> 17)) * 5) ^ C6) * 3) ^ BF ^ (BB & ~B8) ^ M2;
        //B2 = ~((B2 << 1) | (B2 >>> 31)) ^ AA;
        //AB = ((AB ^ (((AA << 15) | (AA >>> 17)) * 5) ^ C5) * 3)	^ B0 ^ (BC & ~B9) ^ M3;
        //B3 = ~((B3 << 1) | (B3 >>> 31)) ^ AB;
        //A0 = ((A0 ^ (((AB << 15) | (AB >>> 17)) * 5) ^ C4) * 3)	^ B1 ^ (BD & ~BA) ^ M4;
        //B4 = ~((B4 << 1) | (B4 >>> 31)) ^ A0;
        //A1 = ((A1 ^ (((A0 << 15) | (A0 >>> 17)) * 5) ^ C3) * 3)	^ B2 ^ (BE & ~BB) ^ M5;
        //B5 = ~((B5 << 1) | (B5 >>> 31)) ^ A1;
        //A2 = ((A2 ^ (((A1 << 15) | (A1 >>> 17)) * 5) ^ C2) * 3)	^ B3 ^ (BF & ~BC) ^ M6;
        //B6 = ~((B6 << 1) | (B6 >>> 31)) ^ A2;
        //A3 = ((A3 ^ (((A2 << 15) | (A2 >>> 17)) * 5) ^ C1) * 3)	^ B4 ^ (B0 & ~BD) ^ M7;
        //B7 = ~((B7 << 1) | (B7 >>> 31)) ^ A3;
        //A4 = ((A4 ^ (((A3 << 15) | (A3 >>> 17)) * 5) ^ C0) * 3)	^ B5 ^ (B1 & ~BE) ^ M8;
        //B8 = ~((B8 << 1) | (B8 >>> 31)) ^ A4;
        //A5 = ((A5 ^ (((A4 << 15) | (A4 >>> 17)) * 5) ^ CF) * 3)	^ B6 ^ (B2 & ~BF) ^ M9;
        //B9 = ~((B9 << 1) | (B9 >>> 31)) ^ A5;
        //A6 = ((A6 ^ (((A5 << 15) | (A5 >>> 17)) * 5) ^ CE) * 3)	^ B7 ^ (B3 & ~B0) ^ MA;
        //BA = ~((BA << 1) | (BA >>> 31)) ^ A6;
        //A7 = ((A7 ^ (((A6 << 15) | (A6 >>> 17)) * 5) ^ CD) * 3)	^ B8 ^ (B4 & ~B1) ^ MB;
        //BB = ~((BB << 1) | (BB >>> 31)) ^ A7;
        //A8 = ((A8 ^ (((A7 << 15) | (A7 >>> 17)) * 5) ^ CC) * 3)	^ B9 ^ (B5 & ~B2) ^ MC;
        //BC = ~((BC << 1) | (BC >>> 31)) ^ A8;
        //A9 = ((A9 ^ (((A8 << 15) | (A8 >>> 17)) * 5) ^ CB) * 3)	^ BA ^ (B6 & ~B3) ^ MD;
        //BD = ~((BD << 1) | (BD >>> 31)) ^ A9;
        //AA = ((AA ^ (((A9 << 15) | (A9 >>> 17)) * 5) ^ CA) * 3) ^ BB ^ (B7 & ~B4) ^ ME;
        //BE = ~((BE << 1) | (BE >>> 31)) ^ AA;
        //AB = ((AB ^ (((AA << 15) | (AA >>> 17)) * 5) ^ C9) * 3)	^ BC ^ (B8 & ~B5) ^ MF;
        //BF = ~((BF << 1) | (BF >>> 31)) ^ AB;

        AB += C6 + CA + CE;
        AA += C5 + C9 + CD;
        A9 += C4 + C8 + CC;
        A8 += C3 + C7 + CB;
        A7 += C2 + C6 + CA;
        A6 += C1 + C5 + C9;
        A5 += C0 + C4 + C8;
        A4 += CF + C3 + C7;
        A3 += CE + C2 + C6;
        A2 += CD + C1 + C5;
        A1 += CC + C0 + C4;
        A0 += CB + CF + C3;

        uint tmp;
        tmp = B0; B0 = C0 - M0; C0 = tmp;
        tmp = B1; B1 = C1 - M1; C1 = tmp;
        tmp = B2; B2 = C2 - M2; C2 = tmp;
        tmp = B3; B3 = C3 - M3; C3 = tmp;
        tmp = B4; B4 = C4 - M4; C4 = tmp;
        tmp = B5; B5 = C5 - M5; C5 = tmp;
        tmp = B6; B6 = C6 - M6; C6 = tmp;
        tmp = B7; B7 = C7 - M7; C7 = tmp;
        tmp = B8; B8 = C8 - M8; C8 = tmp;
        tmp = B9; B9 = C9 - M9; C9 = tmp;
        tmp = BA; BA = CA - MA; CA = tmp;
        tmp = BB; BB = CB - MB; CB = tmp;
        tmp = BC; BC = CC - MC; CC = tmp;
        tmp = BD; BD = CD - MD; CD = tmp;
        tmp = BE; BE = CE - ME; CE = tmp;
        tmp = BF; BF = CF - MF; CF = tmp;
      }

      state[0] = A0;
      state[1] = A1;
      state[2] = A2;
      state[3] = A3;
      state[4] = A4;
      state[5] = A5;
      state[6] = A6;
      state[7] = A7;
      state[8] = A8;
      state[9] = A9;
      state[10] = AA;
      state[11] = AB;

      state[12] = B0;
      state[13] = B1;
      state[14] = B2;
      state[15] = B3;
      state[16] = B4;
      state[17] = B5;
      state[18] = B6;
      state[19] = B7;
      state[20] = B8;
      state[21] = B9;
      state[22] = BA;
      state[23] = BB;
      state[24] = BC;
      state[25] = BD;
      state[26] = BE;
      state[27] = BF;

      state[28] = C0;
      state[29] = C1;
      state[30] = C2;
      state[31] = C3;
      state[32] = C4;
      state[33] = C5;
      state[34] = C6;
      state[35] = C7;
      state[36] = C8;
      state[37] = C9;
      state[38] = CA;
      state[39] = CB;
      state[40] = CC;
      state[41] = CD;
      state[42] = CE;
      state[43] = CF;
    }

    /// <summary>
    /// @see Digest
    /// </summary>
    /// <returns></returns>
    public override String ToString()
    {
      return "Shabal-" + (GetDigestLength() << 3);
    }
  }
}
