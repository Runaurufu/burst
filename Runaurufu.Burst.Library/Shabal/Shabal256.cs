﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Runaurufu.Burst.Library.Shabal
{
  /// <summary>
  /// 
  /// </summary>
  public class Shabal256 : ShabalGeneric
  {
    /// <summary>
    /// 
    /// </summary>
    public Shabal256() : base(256)
    {
    }

    /** @see ShabalGeneric */
    protected override ShabalGeneric Dup()
    {
      return new Shabal256();
    }
  }
}
