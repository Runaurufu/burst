﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Runaurufu.Burst.Library.Shabal
{
  /// <summary>
  /// 
  /// </summary>
  public interface IDigest
  {
    /// <summary>
    /// 
    /// </summary>
    /// <param name="inbuf"></param>
    void Update(byte inbuf);
    /// <summary>
    /// 
    /// </summary>
    /// <param name="inbuf"></param>
    void Update(byte[] inbuf);
    /// <summary>
    /// 
    /// </summary>
    /// <param name="inbuf"></param>
    /// <param name="off"></param>
    /// <param name="len"></param>
    void Update(byte[] inbuf, int off, int len);
    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>
    byte[] Digest();
    /// <summary>
    /// 
    /// </summary>
    /// <param name="inbuf"></param>
    /// <returns></returns>
    byte[] Digest(byte[] inbuf);
    /// <summary>
    /// 
    /// </summary>
    /// <param name="outbuf"></param>
    /// <param name="off"></param>
    /// <param name="len"></param>
    /// <returns></returns>
    int Digest(byte[] outbuf, int off, int len);
    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>
    int GetDigestLength();
    /// <summary>
    /// 
    /// </summary>
    void Reset();
    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>
    IDigest Copy();
    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>
    int GetBlockLength();
    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>
    String ToString();
  }
}
