﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Xml;
using System.Xml.Linq;

namespace Runaurufu.Burst.Library.Mining
{
  //public class PoolMiner : Miner
  //{

  //}


  //    public void Run(Miner miner)
  //    {
  //      const MinerConfig* config = miner->getConfig();
  //  this->miner = miner;
  //  std::string remoteIP = MinerProtocol::resolveHostname(config->poolHost);
  //  if (remoteIP != "")
  //  {
  //    this->running = true;
  //    this->miningInfoSocket.setRemote(remoteIP, config->poolPort);
  //    this->nonceSubmitterSocket.setRemote(remoteIP, config->poolPort);
  //    while (this->running)
  //    {
  //      this->getMiningInfo();
  //      std::this_thread::sleep_for(std::chrono::milliseconds(1500)); // #Runaurufu: for faster new block detection
  //    }
  //  }
  //    }

  //    uint64_t Burst::MinerProtocol::submitNonce(uint64_t nonce, uint64_t accountId)
  //{
  //  NxtAddress addr(accountId);
  //  MinerLogger::write("submitting nonce " + std::to_string(nonce) + " for " + addr.to_string());
  //  std::string request = "requestType=submitNonce&nonce=" + std::to_string(nonce) + "&accountId=" + std::to_string(accountId) + "&secretPhrase=cryptoport";
  //  std::string url = "/burst?" + request;

  //  size_t tryCount = 0;
  //  std::string response = "";
  //  do{
  //    response = this->nonceSubmitterSocket.httpPost(url, "");
  //    tryCount++;
  //  } while (response.empty() && tryCount < this->miner->getConfig()->submissionMaxRetry);

  //  MinerLogger::write(response);
  //  rapidjson::Document body;
  //  body.Parse<0>(response.c_str());

  //  if (body.GetParseError() == nullptr)
  //  {
  //    if (body.HasMember("deadline"))
  //    {
  //      return body["deadline"].GetUint64();
  //    }
  //  }
  //  return (uint64_t)(-1);
  //}

  //    void Burst::MinerProtocol::getMiningInfo()
  //{
  //  std::string response = this->miningInfoSocket.httpPost("/burst?requestType=getMiningInfo", "");

  //  if (response.length() > 0)
  //  {
  //    rapidjson::Document body;
  //    body.Parse<0>(response.c_str());

  //    if (body.GetParseError() == nullptr)
  //    {
  //      if (body.HasMember("baseTarget"))
  //      {
  //        std::string baseTargetStr = body["baseTarget"].GetString();
  //        this->currentBaseTarget = std::stoull(baseTargetStr);
  //      }

  //      if (body.HasMember("generationSignature"))
  //      {
  //        this->gensig = body["generationSignature"].GetString();
  //      }

  //      if (body.HasMember("height"))
  //      {
  //        std::string newBlockHeightStr = body["height"].GetString();
  //        uint64_t newBlockHeight = std::stoull(newBlockHeightStr);
  //        if (newBlockHeight > this->currentBlockHeight)
  //        {
  //          this->miner->updateGensig(this->gensig, newBlockHeight, this->currentBaseTarget);
  //        }
  //        this->currentBlockHeight = newBlockHeight;
  //      }
  //    }
  //  }
  //}

  //std::string Burst::MinerProtocol::resolveHostname(const std::string host)
  //{
  //  struct hostent *he;
  //  struct in_addr **addr_list;
  //  int i;

  //  MinerLogger::write("resolving hostname " + host);

  //  if ((he = gethostbyname(host.c_str())) == NULL)
  //  {
  //    MinerLogger::write("error while resolving hostname");
  //    return "";
  //  }

  //  addr_list = (struct in_addr **) he->h_addr_list;

  //  char inetAddrStr[64];
  //  for (i = 0; addr_list[i] != NULL; i++)
  //  {
  //    std::string ip = std::string(inet_ntop(AF_INET, addr_list[i], inetAddrStr, 64));
  //    MinerLogger::write("Remote IP " + ip);
  //    return ip;
  //  }

  //  MinerLogger::write("can not resolve hostname " + host);
  //  return "";
  //}



  //    public string HttpRequest(string method, string url, string body, string header)
  //    {
  //      String response = null;



  //       IPAddress[] addrs = Dns.Resolve("http addr").AddressList;
  //      IPEndPoint endPoint = new IPEndPoint(addrs[0], 80); // the ip

  //      TcpClient client = new TcpClient();
  //      // timeouts
  //      client.ReceiveTimeout = 5000;
  //      client.SendTimeout = 5000;
  //      client.Connect(addrs, 80);


  //      string request = method + " " + url + " HTTP/1.0\r\n" + header + "\r\n\r\n" + body;

  //      Byte[] byteArray;
  //      byteArray = Encoding.UTF8.GetBytes(request);

  //      NetworkStream stream = client.GetStream();
  //      stream.Write(byteArray, 0, byteArray.Length);

  //      Byte[] readBuffer = new Byte[1024];

  //      int totalBytesRead = 0;
  //      int bytesRead = 0;
  //      do
  //      {
  //        bytesRead = stream.Read(byteArray,0, readBuffer.Length - totalBytesRead - 1);
  //        if (bytesRead > 0)
  //        {
  //          totalBytesRead += bytesRead;
  //          readBuffer[totalBytesRead] = 0;
  //          response = Encoding.UTF8.GetString(readBuffer);
  //          totalBytesRead = 0;
  //          readBuffer[totalBytesRead] = 0;
  //        }
  //      } while ((bytesRead > 0) && (totalBytesRead < readBuffer.Length - 1));

  //      client.Close();


  //      if (response.Length > 0)
  //      {
  //        int httpBodyPos = response.IndexOf("\r\n\r\n");
  //        response = response.Substring(httpBodyPos + 4, response.Length - (httpBodyPos + 4));
  //      }



  //      //HttpWebRequest webRequest = (HttpWebRequest)HttpWebRequest.Create(url);

  //      //  // additional header values like 
  //      //  request.UserAgent = "Runaurufu's ARS Communication library";
  //      //  //   webRequest.Referer = "http://runaurufu.com/";
  //      //  request.ContentType = "application/x-www-form-urlencoded";
  //      //  // webRequest.ContentLength = postArray.Length;
  //      //  request.Method = "POST";



  //      return response;
  //    }
  //  }
}
