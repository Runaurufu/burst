﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Runaurufu.Burst.Library.Nxt;

namespace Runaurufu.Burst.Library.Mining
{
  /// <summary>
  /// 
  /// </summary>
  public class MinerWalletCommit
  {
    /// <summary>
    /// 
    /// </summary>
    /// <param name="height"></param>
    /// <param name="accountId"></param>
    /// <param name="nonce"></param>
    /// <param name="deadline"></param>
    public MinerWalletCommit(MiningInfo miningInfo, NxtAddressStub account, ulong nonce, ulong deadline)
    {
      this.MiningInfo = miningInfo;
      this.Account = account;
      this.Nonce = nonce;
      this.Deadline = deadline;
    }

    /// <summary>
    /// 
    /// </summary>
    public MiningInfo MiningInfo { get; private set; }
    /// <summary>
    /// 
    /// </summary>
    public NxtAddressStub Account { get; private set; }
    /// <summary>
    /// 
    /// </summary>
    public ulong Nonce { get; private set; }
    /// <summary>
    /// 
    /// </summary>
    public ulong Deadline { get; private set; }
  }
}
