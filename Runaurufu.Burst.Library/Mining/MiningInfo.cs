﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Runaurufu.Burst.Library.Mining
{
  /// <summary>
  /// 
  /// </summary>
  public class MiningInfo
  {
    /// <summary>
    /// 
    /// </summary>
    /// <param name="creation"></param>
    /// <param name="baseTarget"></param>
    /// <param name="height"></param>
    /// <param name="generationSignature"></param>
    /// <param name="targetDeadline"></param>
    public MiningInfo(DateTime creation, ulong baseTarget, ulong height, string generationSignature, ulong targetDeadline)
    {
      this.DateTime = creation;
      this.BaseTarget = baseTarget;
      this.Height = height;
      this.GenerationSignatureString = generationSignature;
      this.TargetDeadline = targetDeadline;

      this.GenerationSignature = BurstHelper.CalculateGenSig(this.GenerationSignatureString);
      this.ScoopNumber = BurstHelper.CalculateScoopNumber(this.GenerationSignature, this.Height, this.BaseTarget);
    }
    /// <summary>
    /// 
    /// </summary>
    public DateTime DateTime { get; private set; }
    /// <summary>
    /// 
    /// </summary>
    public ulong BaseTarget { get; private set; }
    /// <summary>
    /// 
    /// </summary>
    public ulong Height { get; private set; }
    /// <summary>
    /// 
    /// </summary>
    public string GenerationSignatureString { get; private set; }
    /// <summary>
    /// 
    /// </summary>
    public byte[] GenerationSignature { get; private set; }
    /// <summary>
    /// Maximum deadline which will be accepted by wallet.
    /// </summary>
    public ulong TargetDeadline { get; private set; }
    /// <summary>
    /// 
    /// </summary>
    public int ScoopNumber { get; private set; }
  }
}
