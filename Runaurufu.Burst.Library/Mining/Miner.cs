﻿using Runaurufu.Burst.Library.Nxt;
using Runaurufu.Threading;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Runaurufu.Burst.Library.Mining
{
  public delegate void MiningWorkerTaskProgress(Miner miner, string plotFile, ulong readNonces, ulong totalNonces);
  public delegate void MiningWorkerTaskAborted(Miner miner, string plotFile, ulong readNonces, ulong totalNonces);
  public delegate void MiningWorkerTaskPerformed(Miner miner, string plotFile);
  public delegate void MinerRoundCompleted(Miner miner, MiningInfo info);

  /// <summary>
  /// 
  /// </summary>
  public class Miner
  {
    private MinerWallet wallet;
    private Dictionary<PlotDirectoryData, MinerData> data;

    public event MiningWorkerTaskProgress MinerTaskProgress;
    public event MiningWorkerTaskAborted MinerTaskAborted;
    public event MiningWorkerTaskPerformed MinerTaskPerformed;
    public event MinerRoundCompleted MinerRoundCompleted;

    private void RaiseMinerTaskProgress(string plotFile, ulong readNonces, ulong totalNonces)
    {
      if (this.MinerTaskProgress != null)
        this.MinerTaskProgress(this, plotFile, readNonces, totalNonces);
    }

    private void RaiseMinerTaskAborted(string plotFile, ulong readNonces, ulong totalNonces)
    {
      if (this.MinerTaskAborted != null)
        this.MinerTaskAborted(this, plotFile, readNonces, totalNonces);
    }

    private void RaiseMinerTaskPerformed(string plotFile)
    {
      if (this.MinerTaskPerformed != null)
        this.MinerTaskPerformed(this, plotFile);
    }

    private void RaiseMinerRoundCompleted(MiningInfo info)
    {
      if (this.MinerRoundCompleted != null)
        this.MinerRoundCompleted(this, info);
    }

    /// <summary>
    /// 
    /// </summary>
    public int MaxWorkerThreads { get; set; }
    /// <summary>
    /// 
    /// </summary>
    public int MaxWorkerThreadsPerPlotDirectory { get; set; }
    /// <summary>
    /// 
    /// </summary>
    public int MaxWorkerThreadsPerPlotFile { get; set; }
    /// <summary>
    /// 
    /// </summary>
    public ulong MinimumWorkerChunkSize { get; set; }
    /// <summary>
    /// 
    /// </summary>
    public bool SinglePlotDirectoryRead { get; set; }
    /// <summary>
    /// 
    /// </summary>
    public UInt64 MaxDeadlineCommitAllowed { get; set; }
    /// <summary>
    /// 
    /// </summary>
    public ulong MaxReadBufferSize { get; set; }
    /// <summary>
    /// Get best deadline found this round.
    /// </summary>
    public UInt64 CurrentRoundBestDeadline { get; set; }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="wallet"></param>
    /// <param name="plotDirectories"></param>
    public Miner(MinerWallet wallet, string[] plotDirectories)
    {
      // default settings
      this.MaxWorkerThreads = 0;
      this.MaxWorkerThreadsPerPlotDirectory = 0;
      this.MaxWorkerThreadsPerPlotFile = 0;
      this.MinimumWorkerChunkSize = 0;

      this.SinglePlotDirectoryRead = false;
      this.MaxDeadlineCommitAllowed = 0;
      this.MaxReadBufferSize = 0;


      this.wallet = wallet;
      this.data = new Dictionary<PlotDirectoryData, MinerData>();

      foreach (string directory in plotDirectories)
        data.Add(new PlotDirectoryData(directory), new MinerData());
    }

    /// <summary>
    /// Start new round basing on received info.
    /// </summary>
    /// <param name="info"></param>
    public void Start(MiningInfo info)
    {
      // clear currently scheduled tasks and shut down working workers
      lock (this.data)
      {
        foreach (KeyValuePair<PlotDirectoryData, MinerData> pair in this.data)
        {
          pair.Value.ClearScheduledTasks();
          pair.Value.StopAllWorkers();
        }
      }

      // prepare tasks
      this.PrepareTasks(info);

      // run tasks
      this.StartWorkers();
    }

    private void PrepareTasks(MiningInfo info)
    {
      lock (this.data)
      {
        ulong minimumNoncesCount = this.MinimumWorkerChunkSize * 2;

        foreach (KeyValuePair<PlotDirectoryData, MinerData> pair in this.data)
        {
          if (Directory.Exists(pair.Key.DirectoryPath) == false)
            continue;

          PlotFileData[] plotFiles = pair.Key.PlotFiles;
          foreach (PlotFileData plotFile in plotFiles)
          {
            if (this.MinimumWorkerChunkSize > 0 && minimumNoncesCount <= plotFile.NonceCount)
            {
              ulong workers = plotFile.NonceCount / this.MinimumWorkerChunkSize;
              ulong noncesPerWorker = plotFile.NonceCount / workers;
              ulong noncesForFirstWorker = plotFile.NonceCount - ((workers - 1) * noncesPerWorker);

              WorkerParameters param = new WorkerParameters(plotFile, plotFile.NonceStart, noncesForFirstWorker, info, pair.Value);
              pair.Value.Tasks.Enqueue(param);

              noncesForFirstWorker += plotFile.NonceStart;

              for (ulong i = 1; i < workers; i++)
              {
                param = new WorkerParameters(plotFile, noncesForFirstWorker, noncesPerWorker, info, pair.Value);
                pair.Value.Tasks.Enqueue(param);
                noncesForFirstWorker += noncesPerWorker;
              }
            }
            else
            {
              WorkerParameters param = new WorkerParameters(plotFile, info, pair.Value);
              pair.Value.Tasks.Enqueue(param);
            }
          }
        }
      }
    }

    private void StartWorkers()
    {
      lock (this.data)
      {
        if (this.data.Count == 0)
          return;

        int currentThreads = 0;
        int tasksLeft = 0;
        foreach (KeyValuePair<PlotDirectoryData, MinerData> pair in this.data)
        {
          currentThreads += pair.Value.Workers.Count(w => w.IsStopped == false);
          tasksLeft += pair.Value.Tasks.Count;
        }

        foreach (KeyValuePair<PlotDirectoryData, MinerData> pair in this.data)
        {
          bool totalCountOk = MaxWorkerThreads == 0 || currentThreads < this.MaxWorkerThreads;
          bool plotCountOk = totalCountOk && (MaxWorkerThreadsPerPlotDirectory == 0 || pair.Value.Workers.Count(w => w.IsStopped == false) < this.MaxWorkerThreadsPerPlotDirectory);

          while (totalCountOk && plotCountOk && pair.Value.Tasks.Count > 0)
          {
            WorkerParameters param = pair.Value.Tasks.Dequeue();
            tasksLeft--;

            ThreadWorker worker = new ThreadWorker(MiningThread);
            worker.ThreadWorkerStopped += worker_ThreadWorkerStopped;

            pair.Value.Workers.Add(worker);
            worker.Start(param);
            worker.Name = "Mining thread";

            currentThreads++;

            totalCountOk = MaxWorkerThreads == 0 || currentThreads < this.MaxWorkerThreads;
            plotCountOk = totalCountOk && (MaxWorkerThreadsPerPlotDirectory == 0 || pair.Value.Workers.Count(w => w.IsStopped == false) < this.MaxWorkerThreadsPerPlotDirectory);
          }
        }

        //Console.WriteLine("{0}: Working: {1} Awaiting: {2}", this.wallet.Address, currentThreads, tasksLeft);
      }
    }

    void worker_ThreadWorkerStopped(ThreadWorker worker, object obj = null)
    {
      worker.ThreadWorkerStopped -= worker_ThreadWorkerStopped;
      WorkerParameters param = obj as WorkerParameters;
      if (param == null)
        return;

      PlotDirectoryData directory = param.PlotFile.Directory;

      lock (this.data)
      {
        if (this.data.ContainsKey(directory))
        {
          this.data[directory].Workers.Remove(worker);

          if (this.data[directory].Workers.Count == 0 && this.data[directory].Tasks.Count == 0)
          {
            this.RaiseMinerRoundCompleted(param.Info);
            Console.WriteLine("{0:HH:mm:ss}: Miner completed for {1}", DateTime.Now, this.wallet.Address);
          }
        }
        else
        {
          foreach (KeyValuePair<PlotDirectoryData, MinerData> pair in this.data)
          {
            if (pair.Value.Workers.Contains(worker))
            {
              pair.Value.Workers.Remove(worker);
              break;
            }
          }
        }
      }

      this.StartWorkers();
    }

    private const int FILE_FLAG_NO_BUFFERING = 0x20000000;

    private void MiningThread(ThreadWorkerState workerState, object obj)
    {
      WorkerParameters param = obj as WorkerParameters;
      if (param == null)
        return;

      if (File.Exists(param.PlotFile.FilePath) == false)
        return;

      Int64 sectorSize = param.PlotFile.GetSectorSize();
      if (sectorSize == 0)
        return;

      UInt64 nonceStart = param.NonceStart;// param.PlotFile.NonceStart;
      UInt64 nonceCount = param.NonceCount;// param.PlotFile.NonceCount;
      UInt64 staggerSize = param.PlotFile.StaggerSize;

      Int64 scoopNum = param.Info.ScoopNumber;
      Byte[] genSig = param.Info.GenerationSignature;

      Int64 bufferSize = (long)(staggerSize * BurstHelper.ScoopSize);

      if (this.MaxReadBufferSize > 0)
        bufferSize = Math.Min(bufferSize, (long)(this.MaxReadBufferSize * BurstHelper.ScoopSize));

      Int64 startByte = scoopNum * BurstHelper.ScoopSize * (long)staggerSize;
      // on which chunk start reading
      UInt64 chunkNum = (ulong)((nonceStart - param.PlotFile.NonceStart) / staggerSize); //0;
      UInt64 totalChunk = param.PlotFile.NonceCount / staggerSize;

      UInt64 noncesInBuffer = (ulong)(bufferSize / BurstHelper.ScoopSize);

      UInt64 nonceOffset = (ulong)chunkNum * staggerSize; //0;
      ulong nonceReadInChunk = (nonceStart - param.PlotFile.NonceStart) - nonceOffset;
      nonceOffset += nonceReadInChunk;

      UInt64 nonceRead = 0;

      UInt64 bestPlotDeadline = UInt64.MaxValue;
      Runaurufu.Burst.Library.Miner.MinerShabal shabal = new Library.Miner.MinerShabal();

      try
      {
        using (FileStream fs = new FileStream(param.PlotFile.FilePath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite, 1024, (FileOptions)FILE_FLAG_NO_BUFFERING))
        {
          while (workerState.IsStopRequested == false &&/* inputStream.good() && */chunkNum <= totalChunk)
          {
            while (nonceReadInChunk < staggerSize)
            {
              Int64 bufferStartRead = startByte + (long)(chunkNum * staggerSize) * BurstHelper.PlotSize + (long)(nonceReadInChunk * BurstHelper.ScoopSize);

              Int64 readPosition;
              if (bufferStartRead % sectorSize != 0)
                readPosition = ((Int64)(bufferStartRead / sectorSize)) * sectorSize;
              else
                readPosition = bufferStartRead;

              Int64 startUselessBytesCount = bufferStartRead - readPosition;
              Int64 fileBufferSize = bufferSize + startUselessBytesCount;
              if (fileBufferSize % sectorSize != 0)
                fileBufferSize = ((Int64)(fileBufferSize / sectorSize) + 1) * sectorSize;

              // check if there is desire to load data outside file size!
              // if (li.QuadPart + fileBufferSize > fileSize.QuadPart)
              //    continue;

              Byte[] fileBuffer = new byte[fileBufferSize + 1];
              int readSize;

              if (this.SinglePlotDirectoryRead)
              {
                lock (param.MinerData.Sync)
                {
                  fs.Position = readPosition;
                  readSize = fs.Read(fileBuffer, 0, (int)fileBufferSize);
                }
              }
              else
              {
                fs.Position = readPosition;
                readSize = fs.Read(fileBuffer, 0, (int)fileBufferSize);
              }

              //       if(chunkNum > 0)
              //         nonceOffset += staggerSize;
              // nonceOffset = (ulong)chunkNum * staggerSize;

              for (uint i = 0; i < noncesInBuffer; i++)
              {
                ulong nonceNumber = param.PlotFile.NonceStart + nonceOffset + i;
                if (nonceNumber < nonceStart + nonceCount && nonceNumber >= nonceStart)
                {
                  //  byte[] test = new byte[BurstHelper.ScoopSize];// == fileBuffer[i]
                  //  Buffer.BlockCopy(fileBuffer, (int)(i * BurstHelper.ScoopSize + startUselessBytesCount), test, 0, (int)BurstHelper.ScoopSize);

                  ulong deadline = BurstHelper.CalculateDeadline(ref shabal, genSig, fileBuffer, (int)(i * BurstHelper.ScoopSize + startUselessBytesCount), param.Info.BaseTarget);

                  if (deadline < bestPlotDeadline)
                  {
                    bestPlotDeadline = deadline;

                    if (this.MaxDeadlineCommitAllowed == 0 || deadline <= this.MaxDeadlineCommitAllowed)
                    {
                      MinerWalletCommit commit = new MinerWalletCommit(param.Info, param.PlotFile.Account, nonceNumber, deadline);
                      this.wallet.PushCommit(commit);
                    }
                  }

                  nonceRead++;
                }
              }

              nonceOffset += noncesInBuffer;
              nonceReadInChunk += noncesInBuffer;

              this.RaiseMinerTaskProgress(param.PlotFile.FilePath, nonceRead, param.NonceCount);
            }
            nonceReadInChunk = 0;
            chunkNum++;
          }
        }
      }
      catch (FileNotFoundException)
      {
        Console.WriteLine("File {0} no longer exists or cannot be accessed!", param.PlotFile.FilePath);
        return;
      }

      if (nonceRead > param.NonceCount)
        Console.WriteLine("WUUT? {0} and should be {1}", nonceRead, param.NonceCount);

      if (workerState.IsStopRequested == true && nonceRead < param.NonceCount)
        this.RaiseMinerTaskAborted(param.PlotFile.FilePath, nonceRead, param.NonceCount);
      else
        this.RaiseMinerTaskPerformed(param.PlotFile.FilePath);

      //    Console.WriteLine("{0}: {1,10:} - {2,10:} nonces for [{3,7:}]", param.PlotFile.AccountId, nonceStart, nonceStart + nonceRead, param.Info.Height);

      // Console.WriteLine("Done {0,7:} with {1} nonces: {2}", param.Info.Height, nonceRead, Path.GetFileName(param.PlotFile.FilePath));
    }

    #region MinerData

    private class MinerData
    {
      public MinerData()
      {
        this.Workers = new List<ThreadWorker>();
        this.Tasks = new Queue<WorkerParameters>();
      }

      public void ClearScheduledTasks()
      {
        this.Tasks.Clear();
      }

      public void StopAllWorkers()
      {
        foreach (ThreadWorker worker in this.Workers)
        {
          worker.Stop();
        }
        this.Workers.Clear();
      }

      public List<ThreadWorker> Workers { get; set; }
      public Queue<WorkerParameters> Tasks { get; set; }
      private readonly object sync = new object();
      public Object Sync { get { return this.sync; } }
    }

    #endregion

    #region WorkerParameters

    private class WorkerParameters
    {
      public WorkerParameters(PlotFileData plotFile, MiningInfo info, MinerData data)
      {
        this.PlotFile = plotFile;
        this.NonceStart = plotFile.NonceStart;
        this.NonceCount = plotFile.NonceCount;
        this.Info = info;
        this.MinerData = data;
      }

      public WorkerParameters(PlotFileData plotFile, UInt64 nonceStart, UInt64 nonceCount, MiningInfo info, MinerData data)
      {
        if (nonceStart < plotFile.NonceStart || nonceStart > plotFile.NonceStart + plotFile.NonceCount)
          throw new ArgumentException("starting nonce is outside of nonce range", "nonceStart");

        if (nonceCount > plotFile.NonceCount || nonceStart + nonceCount > plotFile.NonceStart + plotFile.NonceCount)
          throw new ArgumentException("nonce count points to nonces outside of nonce range", "nonceCount");

        this.PlotFile = plotFile;
        this.NonceStart = nonceStart;
        this.NonceCount = nonceCount;
        this.Info = info;
        this.MinerData = data;
      }

      public PlotFileData PlotFile { get; private set; }
      public UInt64 NonceStart { get; private set; }
      public UInt64 NonceCount { get; private set; }
      public MiningInfo Info { get; private set; }
      public MinerData MinerData { get; private set; }
    }

    #endregion

    #region PlotDirectoryData

    private class PlotDirectoryData
    {
      private readonly object sync = new object();
      public Int64 SectorSize { get; internal set; }

      public Object Sync { get { return this.sync; } }
      public String DirectoryPath { get; private set; }

      private PlotFileData[] plotFiles = null;
      public PlotFileData[] PlotFiles
      {
        get
        {
          lock (this.Sync)
          {
            if (this.plotFiles == null)
              this.RefreshFiles();

            return this.plotFiles;
          }
        }
      }

      public PlotDirectoryData(String directoryPath)
      {
        if (string.IsNullOrWhiteSpace(directoryPath))
          throw new ArgumentNullException("Directory path cannot be null or empty");

        if (Directory.Exists(directoryPath) == false)
          throw new ArgumentException(string.Format("Directory does not exist: {0}", directoryPath), "directoryPath");

        this.DirectoryPath = directoryPath;
      }

      public void RefreshFiles()
      {
        lock (this.Sync)
        {
          List<PlotFileData> plotFilesData = new List<PlotFileData>();
          if (this.plotFiles != null)
          {
            for (int i = 0; i < this.plotFiles.LongLength; i++)
            {
              if (this.plotFiles[i] != null && File.Exists(this.plotFiles[i].FilePath) == true)
                plotFilesData.Add(this.plotFiles[i]);
            }
          }

          string[] allFiles = Directory.GetFiles(this.DirectoryPath);
          foreach (string filePath in allFiles)
          {
            bool alreadyExist = false;
            foreach (PlotFileData item in plotFilesData)
            {
              if (item.FilePath == filePath)
              {
                alreadyExist = true;
                break;
              }
            }

            if (alreadyExist == false)
            {
              try
              {
                PlotFileData newData = new PlotFileData(this, filePath);
                plotFilesData.Add(newData);
              }
              catch (Exception ex)
              {
                Console.WriteLine("Plot file skipped: {0} ({1})", filePath, ex.Message);
              }
            }
          }

          this.plotFiles = plotFilesData.ToArray();
        }
      }
    }

    #endregion

    #region PlotFileData

    private class PlotFileData
    {
      public String FilePath { get; private set; }
      public NxtAddressStub Account { get; private set; }
      public UInt64 NonceStart { get; private set; }
      public UInt64 NonceCount { get; private set; }
      public UInt64 StaggerSize { get; private set; }
      public PlotDirectoryData Directory { get; private set; }

      public PlotFileData(PlotDirectoryData directory, String filePath)
      {
        if (directory == null)
          throw new ArgumentNullException("directory", "directory cannot be null");

        this.Directory = directory;

        if (string.IsNullOrWhiteSpace(filePath))
          throw new ArgumentNullException("File path cannot be null or empty");

        if (File.Exists(filePath) == false)
          throw new ArgumentException(string.Format("File does not exist: {0}", filePath), "filePath");

        this.FilePath = filePath;

        string fileName = Path.GetFileName(this.FilePath);
        string[] fileData = fileName.Split('_');

        if (fileData == null || fileData.Length != 4)
        {
          throw new ArgumentException(string.Format("Unable to parse filename: {0}", this.FilePath), "filePath");
        }

        this.Account = new NxtAddressStub(fileData[0]);

        ulong temp;
        if (UInt64.TryParse(fileData[1], out temp) == false)
        {
          throw new ArgumentException(string.Format("Unable to parse nonce start for: {0}", this.FilePath), "filePath");
        }
        this.NonceStart = temp;

        if (UInt64.TryParse(fileData[2], out temp) == false)
        {
          throw new ArgumentException(string.Format("Unable to parse nonces count for: {0}", this.FilePath), "filePath");
        }
        this.NonceCount = temp;

        if (UInt64.TryParse(fileData[3], out temp) == false)
        {
          throw new ArgumentException(string.Format("Unable to parse stagger size for: {0}", this.FilePath), "filePath");
        }
        this.StaggerSize = temp;
      }

      /// <summary>
      /// Retrieves sector size for current file. If value for this directory was retrieved earlier then it will be returned.
      /// </summary>
      /// <returns></returns>
      public Int64 GetSectorSize()
      {
        lock (this.Directory.Sync)
        {
          if (this.Directory.SectorSize == 0)
          {
            using (FileStream fs = new FileStream(this.FilePath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite, 1024, (FileOptions)FILE_FLAG_NO_BUFFERING))
            {
              for (Int64 check = 2; check <= 16777216; check *= 2)
              {
                try
                {
                  fs.Position = check;
                  this.Directory.SectorSize = check;
                  break;
                }
                catch
                {

                }
              }

              if (this.Directory.SectorSize == 0)
              {
                Console.WriteLine("Could not auto resolve sector size for file: " + this.FilePath);
              }
            }
          }
          return this.Directory.SectorSize;
        }
      }
    }

    #endregion
  }
}
