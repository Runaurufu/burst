﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Runaurufu.Burst.Library.Mining
{
  public class MinerWalletSubmitInfo
  {
    public MinerWalletSubmitInfo(string result, ulong deadline)
    {
      this.Result = result;
      this.Deadline = deadline;
    }

    public string Result { get; private set; }
    public ulong Deadline { get; private set; }
  }
}
