﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Runaurufu.Burst.Library.Mining
{
  /// <summary>
  /// 
  /// </summary>
  public class MinerWalletManager
  {
    /// <summary>
    /// 
    /// </summary>
    public MiningInfo BestMiningInfo { get; set; }

    /// <summary>
    /// 
    /// </summary>
    public event MiningInfoRetrieved NewMiningInfo;

    private void RaiseNewMiningInfo(MinerWallet wallet, MiningInfo info)
    {
      if (this.NewMiningInfo != null)
        this.NewMiningInfo(wallet, info);
    }

    List<MinerWallet> wallets = new List<MinerWallet>();

    /// <summary>
    /// Currently managed wallets
    /// </summary>
    public MinerWallet[] Wallets
    {
      get
      {
        return this.wallets.ToArray();
      }
    }

    /// <summary>
    /// Add wallet to manager
    /// </summary>
    /// <param name="wallet"></param>
    public void AddWallet(MinerWallet wallet)
    {
      if (wallets.Contains(wallet))
        return;

      wallet.Register(this);

      wallet.NewMiningInfo -= wallet_NewMiningInfo;
      wallet.NewMiningInfo += wallet_NewMiningInfo;

      wallets.Add(wallet);
    }

    /// <summary>
    /// Remove wallet from manager
    /// </summary>
    /// <param name="wallet"></param>
    public void RemoveWallet(MinerWallet wallet)
    {
      if (wallets.Contains(wallet) == false)
        return;

      wallets.Remove(wallet);

      wallet.NewMiningInfo -= wallet_NewMiningInfo;

      wallet.Unregister(this);
    }

    /// <summary>
    /// Starts all attached miner wallets
    /// </summary>
    public void Start()
    {
      foreach (MinerWallet wallet in this.wallets)
      {
        wallet.Start();
      }
    }

    /// <summary>
    /// Stops all attached miner wallets
    /// </summary>
    public void Stop()
    {
      foreach (MinerWallet wallet in this.wallets)
      {
        wallet.Stop();
      }
    }

    /// <summary>
    /// Starts mining on all attached miner wallets
    /// </summary>
    public void StartMining()
    {
      foreach (MinerWallet wallet in this.wallets)
      {
        wallet.StartMining();
      }
    }

    /// <summary>
    /// Stops mining on all attached miner wallets
    /// </summary>
    public void StopMining()
    {
      foreach (MinerWallet wallet in this.wallets)
      {
        wallet.StopMining();
      }
    }

    void wallet_NewMiningInfo(MinerWallet wallet, MiningInfo info)
    {
      if (this.BestMiningInfo == null || this.BestMiningInfo.Height < info.Height)
      {
        this.BestMiningInfo = info;
        this.RaiseNewMiningInfo(wallet, info);
      }
    }
  }
}
