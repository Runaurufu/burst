﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Runaurufu.Burst.Library.Miner
{
  /// <summary>
  /// 
  /// </summary>
  public class MinerShabal //: Shabal.Shabal256
  {
    private Shabal.Shabal256 shabal;

    /// <summary>
    /// 
    /// </summary>
    public MinerShabal()
    {
      shabal = new Shabal.Shabal256();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="data"></param>
    public void Update(byte[] data)
    {
      shabal.Update(data);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="data"></param>
    /// <param name="dataStartIndex"></param>
    /// <param name="length"></param>
    public void Update(byte[] data, int dataStartIndex, int length)
    {
      shabal.Update(data, dataStartIndex, length);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="data"></param>
    public void Update(UInt64 data)
    {
      UInt64 swapedData = ReverseBytes(data);
      byte[] byteData = BitConverter.GetBytes(swapedData);

      shabal.Update(byteData);
   //   this.Update(swapedData, sizeof(UInt64));
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="data"></param>
    public void Close(out byte[] data)
    {
      data = shabal.Digest();
      shabal.Reset();//.Close(this.context, out data);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="value"></param>
    /// <returns></returns>
    public static UInt16 ReverseBytes(UInt16 value)
    {
      return (UInt16)((value & 0xFFU) << 8 | (value & 0xFF00U) >> 8);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="value"></param>
    /// <returns></returns>
    public static UInt32 ReverseBytes(UInt32 value)
    {
      return (value & 0x000000FFU) << 24 | (value & 0x0000FF00U) << 8 |
             (value & 0x00FF0000U) >> 8 | (value & 0xFF000000U) >> 24;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="value"></param>
    /// <returns></returns>
    public static UInt64 ReverseBytes(UInt64 value)
    {
      return (value & 0x00000000000000FFUL) << 56 | (value & 0x000000000000FF00UL) << 40 |
             (value & 0x0000000000FF0000UL) << 24 | (value & 0x00000000FF000000UL) << 8 |
             (value & 0x000000FF00000000UL) >> 8 | (value & 0x0000FF0000000000UL) >> 24 |
             (value & 0x00FF000000000000UL) >> 40 | (value & 0xFF00000000000000UL) >> 56;
    }
  }
}
