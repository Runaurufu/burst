﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Runaurufu.Burst.Library.Mining
{
  public class MinerWalletCommitInfo
  {
    public MinerWalletCommitInfo(MinerWalletCommit commit, MinerWalletSubmitInfo submitInfo)
    {
      this.CommitInfo = commit;
      this.SubmitInfo = submitInfo;
    }

    public MinerWalletCommit CommitInfo { get; private set; }
    public MinerWalletSubmitInfo SubmitInfo { get; private set; }
  }
}
