﻿using Runaurufu.Burst.Library.Nxt;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading;
using System.Xml;
using System.Xml.Linq;
using System.IO;
using System.Collections.Concurrent;
using Runaurufu.Threading;

namespace Runaurufu.Burst.Library.Mining
{
  /// <summary>
  /// 
  /// </summary>
  /// <param name="wallet"></param>
  /// <param name="info"></param>
  public delegate void MiningInfoRetrieved(MinerWallet wallet, MiningInfo info);
  /// <summary>
  /// 
  /// </summary>
  /// <param name="wallet"></param>
  /// <param name="commit"></param>
  public delegate void MiningResultCommitted(MinerWallet wallet, MinerWalletCommitInfo commit);

  /// <summary>
  /// 
  /// </summary>
  public class MinerWallet
  {
    private object sync = new object();
    /// <summary>
    /// 
    /// </summary>
    public string Address { get; private set; }
    private int minimumCycleDuration = 2000;

    /// <summary>
    /// Minimum duration of inner get mining info and send commits loop (in msec -> 1sec == 1000msec)
    /// </summary>
    public int MinimumCycleDuration
    {
      get
      {
        lock (this.sync)
        {
          return this.minimumCycleDuration;
        }
      }
      set
      {
        lock (this.sync)
        {
          this.minimumCycleDuration = value;
        }
      }
    }

    private MiningInfo currentMiningInfo;

    private MiningInfo bestMiningInfo;

    /// <summary>
    /// 
    /// </summary>
    /// <param name="address"></param>
    public MinerWallet(string address)
    {
      this.Address = address;
    }

    internal void Register(MinerWalletManager manager)
    {
      manager.NewMiningInfo -= manager_NewMiningInfo;
      manager.NewMiningInfo += manager_NewMiningInfo;
    }

    internal void Unregister(MinerWalletManager manager)
    {
      manager.NewMiningInfo -= manager_NewMiningInfo;
    }

    private void manager_NewMiningInfo(MinerWallet wallet, MiningInfo info)
    {
      if (object.ReferenceEquals(wallet, this))
        return;

      lock (this.sync)
      {
        if (this.bestMiningInfo == null || this.bestMiningInfo.Height < info.Height)
        {
          //foreach (KeyValuePair<ulong, ulong> pair in this.currentHeightDeadline)
          //  Console.WriteLine("{0}: best deadline: {1}", pair.Key, BurstHelper.GetFormatedDeadline(pair.Value));

          //this.currentHeightDeadline.Clear();
          this.bestMiningInfo = info;

          // !! INFORM MINERS !!
          Monitor.PulseAll(this.sync);
        }
      }
    }

    // private Dictionary<ulong, ulong> currentHeightDeadline = new Dictionary<ulong, ulong>();

    // private List<MinerWalletCommit> awaitingCommits = new List<MinerWalletCommit>();
    private ConcurrentStack<MinerWalletCommit> awaitingCommits = new ConcurrentStack<MinerWalletCommit>();

    /// <summary>
    /// 
    /// </summary>
    public event MiningInfoRetrieved NewMiningInfo;

    private void RaiseNewMiningInfo(MiningInfo info)
    {
      if (this.NewMiningInfo != null)
        this.NewMiningInfo(this, info);
    }

    private ThreadWorker infoThread = null;
    private ThreadWorker miningThread = null;

    /// <summary>
    /// 
    /// </summary>
    public void Start()
    {
      lock (this.sync)
      {
        if (this.infoThread != null)
          return;

        this.infoThread = new ThreadWorker(this.InfoThread);
        this.infoThread.Start();
        this.infoThread.Name = "Wallet thread";
      }
    }

    /// <summary>
    /// 
    /// </summary>
    public void Stop()
    {
      lock (this.sync)
      {
        if (this.infoThread == null)
          return;

        this.infoThread.Stop();
      }
    }

    /// <summary>
    /// Is wallet currently mining
    /// </summary>
    public bool IsWorking
    {
      get
      {
        lock (this.sync)
        {
          return this.infoThread != null && this.infoThread.IsRunning;
        }
      }
    }

    void InfoThread(ThreadWorkerState state)
    {
      string resp = string.Empty;
      DateTime cycleStart = DateTime.MinValue;
      using (WebClient client = new WebClient())
      {
        client.CachePolicy = new System.Net.Cache.RequestCachePolicy(System.Net.Cache.RequestCacheLevel.BypassCache);
        client.Headers[HttpRequestHeader.ContentType] = "application/x-www-form-urlencoded";

        this.currentMiningInfo = null;
        Dictionary<ulong, ulong> currentHeightDeadline = new Dictionary<ulong, ulong>();

        while (state.IsStopRequested != true)
        {
          try
          {
            cycleStart = DateTime.UtcNow;

            try
            {
              resp = client.DownloadString("http://" + this.Address + "/burst?requestType=getMiningInfo");
            }
            catch (Exception ex)
            {
              Console.WriteLine("{0}: Unable to obtain mining info ({1})", this.Address, ex.Message);
              continue;
            }

            if (string.IsNullOrWhiteSpace(resp) == false)
            {
              try
              {
                MiningInfo info = ParseMiningInfo(resp);
                if (info != null)
                {
                  if (currentMiningInfo == null
                    || currentMiningInfo.Height < info.Height
                    || (currentMiningInfo.Height == info.Height && currentMiningInfo.GenerationSignature.SequenceEqual(info.GenerationSignature) == false))
                  {
                    currentMiningInfo = info;

                    foreach (KeyValuePair<ulong, ulong> pair in currentHeightDeadline)
                      Console.WriteLine("{0}: best deadline: {1}", pair.Key, BurstHelper.GetFormatedDeadline(pair.Value));

                    currentHeightDeadline.Clear();
                  }
                }
                else
                {
                  Console.WriteLine("{0}: Unable to parse mining info (null info returned)", this.Address);
                  continue;
                }
              }
              catch (Exception ex)
              {
                Console.WriteLine("{0}: Unable to parse mining info ({1})", this.Address, ex.Message);
                continue;
              }

              bool raiseEvent = false;

              lock (this.sync)
              {
                if (this.bestMiningInfo == null
                  || this.bestMiningInfo.Height < currentMiningInfo.Height
                  || (this.bestMiningInfo.Height == currentMiningInfo.Height && this.bestMiningInfo.GenerationSignature.SequenceEqual(currentMiningInfo.GenerationSignature) == false))
                {
                  foreach (KeyValuePair<ulong, ulong> pair in currentHeightDeadline)
                    Console.WriteLine("{0}: best deadline: {1}", pair.Key, BurstHelper.GetFormatedDeadline(pair.Value));

                  currentHeightDeadline.Clear();
                  this.bestMiningInfo = currentMiningInfo;
                  raiseEvent = true;
                }
                //else if (this.miningInfo == null
                //  || this.miningInfo.Height < info.Height
                //  || (this.miningInfo.Height == info.Height && this.miningInfo.GenerationSignature.SequenceEqual(info.GenerationSignature) == false))
                //{
                //  foreach (KeyValuePair<ulong, ulong> pair in this.currentHeightDeadline)
                //    Console.WriteLine("{0}: best deadline: {1}", pair.Key, BurstHelper.GetFormatedDeadline(pair.Value));

                //  this.currentHeightDeadline.Clear();
                //  this.miningInfo = info;
                //  raiseEvent = true;
                //}
              }

              if (raiseEvent)
              {
                this.RaiseNewMiningInfo(currentMiningInfo);

                lock (this.sync)
                  Monitor.PulseAll(this.sync);
              }
            }

            List<MinerWalletCommit> currentAwaitingCommits = new List<MinerWalletCommit>();
            List<MinerWalletCommit> commits = new List<MinerWalletCommit>();
            List<MinerWalletCommit> nextBlocksCommits = new List<MinerWalletCommit>();

            MinerWalletCommit poppedCommit;
            while (this.awaitingCommits.TryPop(out poppedCommit))
            {
              currentAwaitingCommits.Add(poppedCommit);
            }

            foreach (MinerWalletCommit commit in currentAwaitingCommits)
            {
              if (commit.MiningInfo.Height > currentMiningInfo.Height)
              {
                nextBlocksCommits.Add(commit);
                continue;
              }
              else if (commit.MiningInfo.Height < currentMiningInfo.Height)
                continue;

              if (currentHeightDeadline.ContainsKey(commit.Account.AddressId) && currentHeightDeadline[commit.Account.AddressId] < commit.Deadline)
                continue;

              bool addToCommits = true;

              for (int i = commits.Count - 1; i >= 0; i--)
              {
                if (commits[i].Account.AddressId == commit.Account.AddressId)
                {
                  if (commits[i].Deadline > commit.Deadline)
                    commits.RemoveAt(i);
                  else
                  {
                    addToCommits = false;
                    break;
                  }
                }
              }
              if (addToCommits)
                commits.Add(commit);
            }

            currentAwaitingCommits = null;

            if (nextBlocksCommits.Count > 0)
              this.awaitingCommits.PushRange(nextBlocksCommits.ToArray());

            foreach (MinerWalletCommit commit in commits)
            {
              if (this.currentMiningInfo.TargetDeadline != 0 && this.currentMiningInfo.TargetDeadline < commit.Deadline)
              {
                Console.WriteLine("{0}: Deadline ({1}) was discarded due required target deadline ({2})", this.Address, commit.Deadline, this.currentMiningInfo.TargetDeadline);
                continue;
              }

              string url = "http://" + this.Address + "/burst?requestType=submitNonce&nonce=" + commit.Nonce + "&accountId=" + commit.Account.AddressId + "&secretPhrase=cryptoport";

              try
              {
                resp = client.UploadString(url, "POST", "");

                if (string.IsNullOrWhiteSpace(resp))
                {
                  Console.WriteLine("{0}: Unable to commit nonce to (empty response)", this.Address);

                  this.awaitingCommits.Push(commit);
                  continue;
                }
              }
              catch (Exception ex)
              {
                if (ex is WebException)
                {
                  WebException wex = (WebException)ex;
                  if (wex.Response != null)
                  {
                    using (Stream responseStream = wex.Response.GetResponseStream())
                    {
                      using (TextReader textReader = new StreamReader(responseStream))
                      {
                        resp = textReader.ReadToEnd();
                      }
                    }
                    Console.WriteLine("{0}: Commit to failed with message: {1}", this.Address, resp);
                    continue;
                  }
                }

                Console.WriteLine("{0}: Unable to commit nonce to ({1})", this.Address, ex.Message);

                this.awaitingCommits.Push(commit);
                continue;
              }

              MinerWalletSubmitInfo submitInfo;

              try
              {
                submitInfo = ParseSubmitInfo(resp);
              }
              catch (Exception ex)
              {
                Console.WriteLine("{0}: Unable to parse commit ({1})", this.Address, ex.Message);

                this.awaitingCommits.Push(commit);
                continue;
              }

              lock (this.sync)
              {
                if (currentHeightDeadline.ContainsKey(commit.Account.AddressId) == false
                  || submitInfo.Deadline < currentHeightDeadline[commit.Account.AddressId])
                {
                  currentHeightDeadline[commit.Account.AddressId] = submitInfo.Deadline;
                }

                Console.WriteLine("Nonce: {0,15} confirmed: {1} found in {2:hh\\:mm\\:ss}", commit.Nonce, BurstHelper.GetFormatedDeadline(submitInfo.Deadline), (DateTime.UtcNow - currentMiningInfo.DateTime));

                if (string.IsNullOrWhiteSpace(submitInfo.Result) == false)
                  Console.WriteLine("      ({0})", submitInfo.Result);

                if (submitInfo.Deadline != commit.Deadline)
                {
                  if (this.bestMiningInfo.Height != commit.MiningInfo.Height)
                    Console.WriteLine("      WARNING (committed for {0} while currently on {1})", commit.MiningInfo.Height, this.bestMiningInfo.Height);
                  else
                  {
                    Console.WriteLine("      WARNING (expected deadline: {0})", BurstHelper.GetFormatedDeadline(commit.Deadline));

                    if (this.bestMiningInfo.GenerationSignature.SequenceEqual(commit.MiningInfo.GenerationSignature) == false)
                      Console.WriteLine("      (best mining info signature not equal to commit signature)");
                    if (this.currentMiningInfo.GenerationSignature.SequenceEqual(commit.MiningInfo.GenerationSignature) == false)
                      Console.WriteLine("      (mining info signature not equal to commit signature)");
                  }
                }
              }

              this.RaiseMinerResultCommitted(new MinerWalletCommitInfo(commit, submitInfo));
            }

          }
          finally
          {
            TimeSpan cycleDuration = DateTime.UtcNow - cycleStart;
            if (state.IsStopRequested == false && cycleDuration.TotalMilliseconds < this.minimumCycleDuration)
              Thread.Sleep((int)(this.minimumCycleDuration - cycleDuration.TotalMilliseconds));
          }
        }
      }
    }

    public event MiningResultCommitted MinerResultCommitted;

    private void RaiseMinerResultCommitted(MinerWalletCommitInfo commit)
    {
      if (this.MinerResultCommitted != null)
        this.MinerResultCommitted(this, commit);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="response"></param>
    /// <returns></returns>
    public static MinerWalletSubmitInfo ParseSubmitInfo(string response)
    {
      XmlReader reader = JsonReaderWriterFactory.CreateJsonReader(Encoding.UTF8.GetBytes(response), new System.Xml.XmlDictionaryReaderQuotas());
      XElement root = XElement.Load(reader);

      string result = string.Empty;
      UInt64 deadline = 0;

      foreach (XNode node in root.Nodes())
      {
        XElement element = node as XElement;
        if (element != null)
        {
          switch (element.Name.LocalName.ToUpper())
          {
            case "RESULT":
              result = element.Value;
              break;
            case "DEADLINE":
              UInt64.TryParse(element.Value, out deadline);
              break;
          }
        }
      }

      return new MinerWalletSubmitInfo(result, deadline);
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="response"></param>
    /// <returns></returns>
    public static MiningInfo ParseMiningInfo(string response)
    {
      XmlReader reader = JsonReaderWriterFactory.CreateJsonReader(Encoding.UTF8.GetBytes(response), new System.Xml.XmlDictionaryReaderQuotas());
      XElement root = XElement.Load(reader);

      ulong baseTarget = 0;
      ulong height = 0;
      ulong targetDeadline = 0;
      string genSign = string.Empty;

      foreach (XNode node in root.Nodes())
      {
        XElement element = node as XElement;
        if (element != null)
        {
          switch (element.Name.LocalName.ToUpper())
          {
            case "BASETARGET":
              UInt64.TryParse(element.Value, out baseTarget);
              break;
            case "HEIGHT":
              UInt64.TryParse(element.Value, out height);
              break;
            case "GENERATIONSIGNATURE":
              genSign = element.Value;
              break;
            case "TARGETDEADLINE":
              UInt64.TryParse(element.Value, out targetDeadline);
              break;
          }
        }
      }

      return new MiningInfo(DateTime.UtcNow, baseTarget, height, genSign, targetDeadline);
    }
    /// <summary>
    /// 
    /// </summary>
    public void StartMining()
    {
      lock (this.sync)
      {
        if (this.miningThread != null && this.miningThread.IsRunning)
          return;

        if (this.miningThread == null)
          this.miningThread = new ThreadWorker(this.MiningThread);
        this.miningThread.Start();
        this.miningThread.Name = "Miner thread";
      }
    }

    /// <summary>
    /// 
    /// </summary>
    public void StopMining()
    {
      lock (this.sync)
      {
        if (this.miningThread == null)
          return;

        this.miningThread.Stop();
      }
    }

    /// <summary>
    /// Is wallet currently mining
    /// </summary>
    public bool IsMining
    {
      get
      {
        lock (this.sync)
        {
          return this.miningThread != null && this.miningThread.IsRunning;
        }
      }
    }

    private List<Miner> miners = new List<Miner>();
    /// <summary>
    /// 
    /// </summary>
    /// <param name="miner"></param>
    public void AddMiner(Miner miner)
    {
      lock (this.sync)
      {
        this.miners.Add(miner);
      }
    }

    void MiningThread(ThreadWorkerState state)
    {
      MiningInfo info = null;
      while (state.IsStopRequested != true)
      {
        lock (this.sync)
        {
          while ((this.bestMiningInfo == null || (info != null && info.Height >= this.bestMiningInfo.Height)) && state.IsStopRequested == false)
          {
            Monitor.Wait(this.sync, 5000);
          }
          info = this.bestMiningInfo;
        }

        if (state.IsStopRequested)
          break;

        if (info == null)
          continue;

        Miner[] miners;
        lock (this.sync)
        {
          miners = this.miners.ToArray();
        }

        foreach (Miner miner in miners)
        {
          miner.Start(info);
        }
      }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="commit"></param>
    public void PushCommit(MinerWalletCommit commit)
    {
      MiningInfo currentInfo = this.currentMiningInfo;
      if (currentInfo != null)
      {
        // check if current wallet already started for searching of new height
        if (currentInfo.Height > commit.MiningInfo.Height)
          return;
      }
      this.awaitingCommits.Push(commit);
    }
  }
}
